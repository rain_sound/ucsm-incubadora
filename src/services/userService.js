import api from "../helpers/api";
import { configFormData } from "../helpers/api";
import { userConstants } from "../constants/user/api";

const login = (username, password) => {
  return api
    .post(userConstants.SIGNIN, {
      username,
      password
    })
    .then(user => {
      localStorage.setItem("user", JSON.stringify(user.data));
      return user.data;
    })
};

const register = (username, password, email, roleId, orcid, dina, dni, cellphone) => {
  return api
    .post(userConstants.SIGNUP, {
      username,
      email,
      password,
      dina,
      dni,
      cellphone,
      roleId,
      orcid
    })
    .then(user => {
      return user.data;
    })
};

const resetPassword = email => {
  return api
    .post(userConstants.FORGOTPASSWORD, {
      email
    })
    .then(res => {
      return res.data;
    })
    .catch(err => {
      const res = err.response;
      const { error, reason, message } = res.data;
      return {
        error,
        reason,
        message
      };
    });
};

const verifyToken = token => {
  return api
    .get(userConstants.RESETPASSWORD, {
      params: {
        token
      }
    })
    .then(res => {
      return res.data;
    })
    .catch(err => {
      const res = err.response;
      const { error, reason } = res.data;
      return {
        error,
        reason
      };
    });
};

const updatePassword = (username, password, oldPassword) => {
  return api
    .put(userConstants.UPDATEPASSWORD, {
      username,
      oldPassword,
      password
    })
    .then(res => {
      return res.data;
    })
    .catch(err => {
      const res = err.response;
      const { error, reason } = res.data;
      return {
        error,
        reason
      };
    });
};

const updateImage = (image, id) => {
  const formData = new FormData();
  formData.append("image", image);
  formData.append("id", id);
  return api
    .post(userConstants.UPDATEIMAGE, formData, configFormData)
    .then(res => {
      return res.data;
    })
    .catch(err => {
      const res = err.response;
      const { error, reason } = res.data;
      return {
        error,
        reason
      };
    });
};

const getAll = () => {
  return api
    .get(userConstants.GET_ALL)
    .then(res => console.log(res))
    .catch(err => console.log(err));
};


const getUsersToApproved = (offset = 0, limit = 10) => {
  return api
  .post(userConstants.GETUSERSTOAPP, {
    offset,
    limit
  })
  .then(res => res.data)
  .catch(err => err);
}

const approveUser = (userId, request) => {
  return api
  .post(userConstants.APPROVEUSER,{
    userId,
    request
  })
  .then(res => res.data)
  .catch(err => err);
}

const getUsersToApprovedFilter = text => {
  return api
  .post(userConstants.GETUSERSTOAPPFILTER, {
    text
  })
  .then(res => res.data)
  .catch(err => err);
}

const getCount = () => {
  return api
    .get(userConstants.GETUSERSCOUT)
    .then(res => res)
    .catch(err => err);
}


export const userService = {
  getAll,
  login,
  register,
  resetPassword,
  verifyToken,
  updatePassword,
  updateImage,
  getUsersToApproved,
  approveUser,
  getUsersToApprovedFilter,
  getCount
};
