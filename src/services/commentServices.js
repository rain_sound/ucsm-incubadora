import api from '../helpers/api'

const addComment = (resp_id, body, userId) => {
    return api.post('entradas/comment/addcomment', {
        resp_id,
        body,
        userId,
    }).then((resp) => {
        if (resp.data === "1")
            console.log("Operacion Exitosa")
        else
            console.log("Falla al registrar")
    }).catch(err => {
        const res = err.response
        const { auth, reason } = res.data
        return {
            auth,
            reason
        }
    })
}

const getPostComments = (resp_id) => {
    return api.get(`entradas/comment/getpostcomments?resp_id=${resp_id}`)
        .then(resp => resp.data)
        .catch(err => {
            const res = err.response
            const { auth, reason } = res.data
            return {
                auth,
                reason
            }
        })
}

const updateComment = (id, body) => {
    return api.post('entradas/comment/updatecomment', {
        id,
        body
    }).then((resp) => {
        if (resp.data === "1")
            console.log("Operacion Exitosa")
        else
            console.log("Falla al registrar")
    }).catch(err => {
        const res = err.response
        const { auth, reason } = res.data
        return {
            auth,
            reason
        }
    })
}
const deleteComment = (id) => {
    return api.post('entradas/comment/deletecomment', {
        id
    }).then((resp) => {
        if (resp.data === "1")
            console.log("Operacion Exitosa")
        else
            console.log("Falla al registrar")
    }).catch(err => {
        const res = err.response
        const { auth, reason } = res.data
        return {
            auth,
            reason
        }
    })
}

export {
    addComment,
    getPostComments,
    updateComment,
    deleteComment,
}