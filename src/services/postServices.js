import api from '../helpers/api'
import { configFormData } from "../helpers/api";

const regPost = (userId, head, body) => {
    return api.post('entradas/post/regpost', {
        head,
        body,
        userId
    }).then((resp) => {
        if (resp.data === "1")
            console.log("Operacion Exitosa")
        else
            console.log("Falla al registrar")
    }).catch(err => {
        const res = err.response
        const { auth, reason } = res.data
        return {
            auth,
            reason
        }
    })
}

const getAllPosts = (offset, limit) => {
    return api.get(`entradas/post/getallposts?offset=${offset}&limit=${limit}`)
        .then(resp => resp.data)
        .catch(err => {
            const res = err.response
            const { auth, reason } = res.data
            return {
                auth,
                reason
            }
        })
}

const getUserPosts = (userId, offset, limit) => {
    return api.get(`entradas/post/getuserposts?userId=${userId}&offset=${offset}&limit=${limit}`)
        .then(resp => {
            return resp.data
        })
        .catch(err => {
            const res = err.response
            const { auth, reason } = res.data
            return {
                auth,
                reason
            }
        })
}

const getPostById = id => {
    return api.get(`entradas/post/getpostbyid?id=${id}`)
        .then(resp => {
            return resp.data
        })
        .catch(err => {
            const res = err.response
            const { auth, reason } = res.data
            return {
                auth,
                reason
            }
        })
}

const updatePost = (id, head, body) => {
    return api.post('entradas/post/updatePost', {
        id,
        head,
        body
    }).then((resp) => {
        if (resp.data === "1")
            console.log("Operacion Exitosa")
        else
            console.log("Falla al registrar")
    }).catch(err => {
        const res = err.response
        const { auth, reason } = res.data
        return {
            auth,
            reason
        }
    })
}

const deletePost = (id) => {
    return api.post('entradas/post/deletepost', {
        id
    }).then((resp) => {
        if (resp.data === "1")
            console.log("Operacion Exitosa")
        else
            console.log("Falla al registrar")
    }).catch(err => {
        const res = err.response
        const { auth, reason } = res.data
        return {
            auth,
            reason
        }
    })
}

const updateImage = (image, id) => {
    const formData = new FormData();
    formData.append("image", image);
    formData.append("id", id);
    return api
        .post('entradas/post/uploadimage', formData, configFormData)
        .then(res => {
            return res.data;
        })
        .catch(err => {
            const res = err.response;
            const { error, reason } = res.data;
            return {
                error,
                reason
            };
        });
};

const getAllPostsSize = () => {
    return api.get('entradas/post/allpostcount')
        .then(resp => {
            return resp.data
        })
        .catch(err => {
            const res = err.response
            const { auth, reason } = res.data
            return {
                auth,
                reason
            }
        })
}

const getUserPostsSize = (userId) => {
    return api.get(`entradas/post/userpostcount?userId=${userId}`)
        .then(resp => {
            return resp.data
        }).catch(err => {
            const res = err.response
            const { auth, reason } = res.data
            return {
                auth,
                reason
            }
        })
}

export {
    regPost,
    getAllPosts,
    getUserPosts,
    getPostById,
    updatePost,
    deletePost,
    updateImage,
    getAllPostsSize,
    getUserPostsSize
}