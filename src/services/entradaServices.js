import api from '../helpers/api'
/*
const crearEntrada = (head, body, user_id) => {
    return api.post('entradas/crearentrada', {
        head,
        body,
        user_id
    }).then((resp) => {
        if (resp.data === "1")
            console.log("Operacion Exitosa")
        else
            console.log("Falla al registrar")
    }).catch(err => {
        const res = err.response
        const { auth, reason } = res.data
        return {
            auth,
            reason
        }
    })
}

const ingresarRespuesta = (resp_id, body, user_id) => {
    return api.post('entradas/ingresarrespuesta', {
        resp_id,
        body,
        user_id,
    }).then((resp) => {
        if (resp.data === "1")
            console.log("Operacion Exitosa")
        else
            console.log("Falla al registrar")
    }).catch(err => {
        const res = err.response
        const { auth, reason } = res.data
        return {
            auth,
            reason
        }
    })
}

const listarEntradas = () => {
    return api.get('entradas/listarentradas')
        .then(resp => resp.data)
        .catch(err => {
            const res = err.response
            const { auth, reason } = res.data
            return {
                auth,
                reason
            }
        })
}

const listarEntradasUsuario = (user_id) => {
    return api.get(`entradas/listarentradasusuario?user_id=${user_id}`)
        .then(resp => {
            return resp.data
        })
        .catch(err => {
            const res = err.response
            const { auth, reason } = res.data
            return {
                auth,
                reason
            }
        })
}

const listarRespuestas = (resp_id) => {
        return api.get(`entradas/listarrespuestas?resp_id=${resp_id}`)
            .then(resp => resp.data)
            .catch(err => {
                const res = err.response
                const { auth, reason } = res.data
                return {
                    auth,
                    reason
                }
            })
    }
    /////////////////////////////////////////////////////////////////*/
const actualizarEntrada = (id, head, body) => {
    debugger
    return api.post('entradas/actualizarentrada', {
        id,
        head,
        body
    }).then((resp) => {
        if (resp.data === "1")
            console.log("Operacion Exitosa")
        else
            console.log("Falla al registrar")
    }).catch(err => {
        const res = err.response
        const { auth, reason } = res.data
        return {
            auth,
            reason
        }
    })
}
const borrarEntrada = (id) => {
    return api.post('entradas/borrarentrada', {
        id
    }).then((resp) => {
        if (resp.data === "1")
            console.log("Operacion Exitosa")
        else
            console.log("Falla al registrar")
    }).catch(err => {
        const res = err.response
        const { auth, reason } = res.data
        return {
            auth,
            reason
        }
    })
}

export {
    // crearEntrada,
    // ingresarRespuesta,
    // listarEntradas,
    // listarEntradasUsuario,
    // listarRespuestas,
    actualizarEntrada,
    borrarEntrada
}