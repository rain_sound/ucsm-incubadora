import api from "../helpers/api";
import { courseConstants } from "../constants/course/api";

const getAllCoursesByTeacher = id => {
  return api
    .get(courseConstants.GET_ALL_COURSES_BY_TEACHER(id))
    .then(courses => {
        return courses.data;
    })
};

const createCourse = (id,  name, description, roles, start_date, end_date) => {
    return api
      .post(courseConstants.CREATE_COURSE, {
          id,
          name,
          description,
          roles,
          start_date,
          end_date
      }).then( course => course.data)
}

const updateCourse = (id, name, description, status) => {
    return api
     .put(courseConstants.UPDATE_COURSE(id),{
         name,
         description,
         status
     })
     .then(course => course.data)
}
export const courseServices = {
    getAllCoursesByTeacher,
    createCourse,
    updateCourse
};
