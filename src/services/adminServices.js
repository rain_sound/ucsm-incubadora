import api from '../helpers/api'


const getAllUsers = () => {
    return api.post('admin/getallusers')
        .then(resp => resp.data)
        .catch(err => err.response)
}

const banUser = (userId, banned) => {
    return api.post('admin/banuser', {
            id: userId,
            banned
        }).then(resp => resp.data)
        .catch(err => err.data)
}

const setRoleUser = (userId, roleId) => {
    return api.post('admin/setroleuser', {
            id: userId,
            roleId
        }).then(resp => resp.data)
        .catch(err => err.response)
}

const setMarkPost = (postId, mark) => {
    return api.post('admin/setmarkpost', {
        id: postId,
        mark: mark
    }).then(resp => resp.data)
    .catch(err => err.response)
}

export {
    getAllUsers,
    banUser,
    setRoleUser,
    setMarkPost,
}