import api from '../helpers/api'


const getAllRoles = () => {
    return api.get('role/getallroles')
        .then(resp => resp.data)
        .catch(err => err.response)
}
export {
    getAllRoles,
}