import { store } from './store/index'
import update from 'immutability-helper'

const { name, rol } = store.getState().user;

var admin  = [{
  name: "Aprobar usuarios",
  url: "/admin/approved/1",
  icon: "icon-check"
}];

const nav =  {
  items: [{
          name: `Bienvenido ${name}`,
          icon: `icon-mustache`,
          attributes: { disabled: true },
          badge: {
              variant: 'success',
              text: 'Online',
          },
      },
      {
          name: 'Perfil',
          url: '/profile',
          icon: 'icon-user',
          children: [{
                  name: 'Password',
                  url: `/profile/${name}/reset`,
                  icon: 'icon-wrench',
              },
              {
                  name: 'Profile Image',
                  url: `/profile/${name}`,
                  icon: 'icon-camera',
              },
          ],
      },
      {
        name: 'Foro',
        url: '/foro',
        icon: 'icon-speech',
        children: [{
                name: 'Posts',
                url: '/foro/1',
                icon: 'icon-speech'
            }, {
                name: 'Registrar Posts',
                url: `/regpost`,
                icon: 'icon-notebook',
            },
            {
                name: 'Mis Post',
                url: `/myposts/1`,
                icon: 'icon-list',
            },
        ],
    }
  ],
}; 

if(rol === '4') {
   nav.items = update(nav.items, { $push: admin })
}


export default nav
