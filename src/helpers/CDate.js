class CDate {
    constructor(){}

    getMonthLong(){
        return this.time.toLocaleString('sp-mx', { month: 'long' })
    }

    getFullDate(){
        return this.time.toDateString('sp-mx')
    }

    setTime(time){
        this.time = new Date(time);
    }

}
export default CDate