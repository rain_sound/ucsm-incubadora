import React from 'react'

export const Forbidden = () => (
    <div>
        Ruta no permitida, debe loguearse primero
    </div>
)