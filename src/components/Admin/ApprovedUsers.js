import React, { Component } from 'react'
import { connect } from 'react-redux'
import { userActions } from '../../actions/user'
import { userService } from '../../services/userService'
import Card from '../Other/Card/Card'
import Pagination from '../Other/Pagination'
import { URL_PUBLIC } from "../../helpers/api";

const PAGE_LIMIT = 6;

class ApprovedUsers extends Component {
    constructor(props){
        super(props)
        this.state= {
            request: null,
        }
    }

    componentDidUpdate(){
        if (this.props.match.params.id != this.state.page){
            this.loadUsers()
            this.setState ({
                page: this.props.match.params.id
            })
        }
      }

      loadUsers = () => {
        let page = parseInt( this.props.match.params.id )
        let limit = PAGE_LIMIT
        var offset = (page-1)*limit
        this.props.getAllUsersAppr(offset, limit)
      }




    componentDidMount = () =>{
        this.props.getCount()
        this.loadUsers()


    }

    handleApprove = (id, request) => {
        userService.approveUser(id, request).then(res => console.log('resultad', res))
    }

    handlChange = e => {
        const { value } = e.target
        let page = parseInt( this.props.match.params.id )
        let limit = PAGE_LIMIT
        var offset = (page-1)*limit
        if(value.length === 0 || value === '')
            this.props.getAllUsersAppr(offset, limit)
        else this.props.getAllUserApprFilter(value)
    }

    content = ({ id, dni, dina, orcid, email, cellphone }) => {
        return (
          <React.Fragment>
            <span className="bold">DNI</span>
            <span className="text-muted">{dni}</span>

            <span className="bold">Dina</span>
            <span className="text-muted">{dina}</span>

            <span className="bold">Orcid</span>
            <span className="text-muted">{orcid}</span>

            <span className="bold">Email</span>
            <span className="text-muted">{email}</span>

            <span className="bold">Celular</span>
            <span className="text-muted">{cellphone}</span>

            <div className="mt-2 row text-center">
              <button onClick={() => this.handleApprove(id, true)} className=" col-4 btn btn-success"> Aceptar </button>
              <button onClick={() => this.handleApprove(id, false)} className="col-4 btn btn-danger"> Rechazar </button>
            </div>
          </React.Fragment>
        );
    }

    render(){
        const { usersToApprov } = this.props
        return(
            <div className="row">
                <input onChange={this.handlChange} className="form-control" placeholder="Buscar usuario" type="text" name="text" id="text"/>
                {
                  usersToApprov &&  usersToApprov.map(item => <Card content_right={this.content(item)} key={item.id} img={`${URL_PUBLIC}${item.imgProfile}`} title={item.name}/> )
                }
                <Pagination />
            </div>
            
        )
    }
}

const mapDispatchToProps = dispatch => ({
    getAllUsersAppr : (offset, limit) => dispatch(userActions.getAllUsersAppr(offset, limit)),
    getAllUserApprFilter: text =>  dispatch(userActions.getAllUserApprFilter(text)),
    getCount: () => dispatch(userActions.getCount())
})

const mapStateToProps = ({ user: { usersToApprov }  }) => ({  usersToApprov })

export default connect(mapStateToProps, mapDispatchToProps)(ApprovedUsers)
