import React, { Component } from 'react';
import UserCard from './UserCard';

class UserList extends Component {
    constructor(){
        super()
        this.state={
            search: ''
        }
        this.updateSearch = this.updateSearch.bind(this)
    }
    updateSearch(event){
        this.setState({
            search: event.target.value.substr(0, 20)
        })
    }
    renderUsers(filteredUsers){
        return filteredUsers.map(item => <UserCard key={item.id} params={item}></UserCard>)
    }
    render() {
        let filteredUsers = this.props.users.filter(
            (user)=>{
                return user.name.toLowerCase().indexOf(this.state.search.toLowerCase()) !== -1 || 
                    user.email.toLowerCase().indexOf(this.state.search.toLowerCase()) !== -1;
            }
        );
        return (
            <div>
                <input class="form-control" type="text" placeholder="Busqueda de Usuario" aria-label="Search"
                value={this.state.search} onChange={this.updateSearch}></input>
                <br />
                <div className="card-columns">
                    {this.props.users&&this.state.search !== '' ? this.renderUsers(filteredUsers):null}
                </div>
            </div>
        );
    }
}

export default UserList;