import React, { Component } from 'react';
import {URL_PUBLIC} from '../../helpers/api'
import {getRolesList} from '../../reducers'
import {adminActions} from  '../../actions/admin'
import {connect} from 'react-redux'


/* props
    user {
        name,
        email,
        imgProfile,
    }
 */

class UserCard extends Component {
    constructor(props){
        super(props)
        let {id, banned, roleId} = this.props.params
        this.state = {
            banned,
            roleId,
            id
        }
        this.renderCheck = this.renderCheck.bind(this)
        this.handleCheck = this.handleCheck.bind(this)
        this.handleSelect = this.handleSelect.bind(this)
        this.renderOptions = this.renderOptions.bind(this)
        this.updateUserState = this.updateUserState.bind(this)
    }
    handleCheck(e){
        let banned = e.target.checked
        this.setState({
            banned
        })
    }
    handleSelect(e){
        this.setState({
            roleId: e.target.value 
        })
    }
    renderCheck(){
        if(this.state.banned)
            return <input class="form-check-input" onChange={this.handleCheck} type="checkbox" value="" id="bannedCheck" checked />
        else
            return <input class="form-check-input" onChange={this.handleCheck} type="checkbox" value="" id="bannedCheck" />
    }
    renderOptions(){
        return this.props.roles.map(r => <option value={r.id} selected={r.id===this.state.roleId}>{r.description}</option>)
    }
    updateUserState(){
        let {banned, roleId, id} = this.state
        this.props.banUser(id, banned)
        this.props.setRole(id, roleId)
    }
    render() {
        let user = this.props.params
        return (
            <div class="card">
                <img src={`${URL_PUBLIC}${user.imgProfile}`} class="card-img-top" alt="User Profile Image"
                style={{borderRadius: "30%", padding: "5px", width: "60%", display: "block", marginLeft: "auto", marginRight: "auto"}} />
                <div class="card-body">
                    <p class="card-text" style={{textAlign: "center"}}>{user.name}</p>
                    <p class="card-text" style={{textAlign: "center"}}>{user.email}</p>
                    {/* Formulario */}
                    <form onSubmit = {this.updateUserState}>
                        <select class="form-control" onChange={this.handleSelect} refs="userRole" id="roleSelector">
                            {/* aplicar un render con .map */}
                            {this.renderOptions()}    
                        </select>
                        <div class="form-check">
                            {this.renderCheck()}
                            <label class="form-check-label" for="bannedCheck">
                                Bann
                            </label>
                        </div>
                        <button type="submit" class="btn btn-primary">Actualizar</button>
                    </form>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    roles: getRolesList(state)
})

const mapDispatchToProps = dispatch => ({
    banUser: (userId, banned) => dispatch(adminActions.banUser(userId, banned)),
    setRole: (userId, roleId) => dispatch(adminActions.setRoleUser(userId, roleId))
})

export default connect(mapStateToProps, mapDispatchToProps)(UserCard);