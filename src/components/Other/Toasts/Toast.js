import React, { Component } from "react";

class Toast extends Component {
  render() {
    return (
      <li className={`toast ${this.props.type === 'error'? 'toast--error':'toast--success'}`}>
        <p className="toast__content">
          {this.props.message}
        </p>
        <button className="toast__dismiss" onClick={this.props.onDismissClick}>
          x
        </button>
      </li>
    );
  }

  shouldComponentUpdate() {
    return false;
  }
}

export default Toast;