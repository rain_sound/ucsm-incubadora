import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter } from "react-router-dom"

const PAGE_LIMIT = 6;
class Pagination extends Component {
    constructor(props){
        super(props);
        let page = parseInt( props.match.params.id )
        
        this.state = {
            page,
        }
        
    }
   
    renderPages = (page, limit) =>{
      var arr = []
      let index = (page === 1)?0:(((page-1)===1)?(page-1):(page-2))
      let lmt = (page === limit)?limit:(page+2)
      for ( index; index < lmt; index++) {  
          arr.push(index+1);          
      }
      return arr.map((n) => {
          if (n === page)
              return <li key= {n} className="page-item active"><a className="page-link" href={`/admin/approved/${n}`}  >{n}</a></li>
          else
              return <li key= {n} className="page-item"><a className="page-link" href={`/admin/approved/${n}`}   >{n}</a></li>
      })
    }

    renderPagination = () => {
        let page = parseInt( this.props.match.params.id )
        var limit = parseInt( this.props.count )
        limit = Math.ceil(limit/PAGE_LIMIT)
        return (
            <nav className="mt-3" aria-label="...">
                <ul className="pagination justify-content-center">

                    <li className={`page-item ${page===1?'disabled':''}`}>
                    <a className="page-link" href={`/admin/approved/${(page-1)}`} tabIndex="-1"  onClick={this.rel}>Previous</a>
                    </li>

                    {this.renderPages(page, limit)}
                  
                    <li className={`page-item ${page===limit?'disabled':''}`}>
                    <a className="page-link" href={`/admin/approved/${(page+1)}`}  onClick={this.rel}>Next</a>
                    </li>

                </ul>
            </nav>
        )
    }


    render() {
        return (
          <React.Fragment>
              <div className="col-12 text-center">
                  {this.renderPagination()}            
              </div>
          </React.Fragment>
        );
    }
}

const mapStateToProps = ({ user: { count }}) => ({ count })
export default withRouter(connect(mapStateToProps, null)(Pagination))
