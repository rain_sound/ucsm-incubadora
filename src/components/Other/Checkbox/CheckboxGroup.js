import React from 'react';
import Checkbox from './Checkbox';

class CheckBoxGroup extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
      const { checkboxes, handleChange, checked } = this.props;
    return (
      <React.Fragment>
        {checkboxes.map(item => (
            <label key={item.key}>
              <Checkbox
                name={item.name}
                value={item.value}
                checked={checked}
                handleChange={name => handleChange(name)}
              />
            </label>
        ))}
      </React.Fragment>
    );
  }
}

export default CheckBoxGroup;