import React from "react";
import PropTypes from "prop-types";

const Checkbox = ({
  type = "checkbox",
  value,
  name,
  checked = false,
  handleChange
}) => {
  return (
    <React.Fragment>
      <input onChange={() => handleChange(name)}  type={type}  id={`cb${name}`} />
      <label className="checkbox-label" htmlFor={`cb${name}`}>
        <img className="checkbox-img" src="http://lorempixel.com/100/100" />
      </label>
    </React.Fragment>
  );
};
Checkbox.propTypes = {
  type: PropTypes.string,
  name: PropTypes.string.isRequired,
  handleChange: PropTypes.func.isRequired
};

export default Checkbox;
