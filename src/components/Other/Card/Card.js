import React from 'react'

const Card = ({ title, img, content, content_right }) => {
    return (
          <div className="mt-3 col-12 col-lg-4 "> 
            <div className="m-auto card-generic">
              <h5 className="card-generic-title text-center">
                { title }
              </h5>
              <div className="row">
                <div className="d-none d-md-block col-md-4 justify-content-between">
                  <div className="row p-3">
                    <div className="col-md-12 align-self-center text-center">
                      <img
                        className="rounded img-fluid"
                        src={img}
                        alt=""
                      />
                    </div>
                    <div className="col-md-8 align-self-center">
                     { content }
                    </div>
                  </div>
                </div>
                <div className="col-md-8 align-self-center p-3">
                  <div className="d-flex flex-column ">
                   { content_right }
                  </div>
                </div>
              </div>
            </div>
          </div>
    );
}

export default Card;