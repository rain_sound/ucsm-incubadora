import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import Card from "../Other/Card/Card";
// for React 16.4.x use: import { ReactTabulator }
import CDate from "../../helpers/CDate";
/* import { ButtonRow } from "../Other/"; */

import { courseActions } from "../../actions/course";

const time = new CDate();

class CourseDetail extends Component {
  componentDidMount = () => {
    const {
      user: { id },
      getAll
    } = this.props;
    getAll(id);

  };

  cardContentRight = () => (
    <React.Fragment>
      <span>
        <strong>45</strong> Estudiantes
      </span>
      <span>Visibilidad</span>
      <div className="text-center">
        <img
          className="img-fluid rounded-circle"
          src="https://picsum.photos/70/70"
          alt=""
        />
        <img
          className="img-fluid rounded-circle"
          src="https://picsum.photos/70/70"
          alt=""
        />
        <img
          className="img-fluid rounded-circle"
          src="https://picsum.photos/70/70"
          alt=""
        />
      </div>
    </React.Fragment>
  );

  cardContent = ({ description, createdAt, id }) => {
    
    return (
      <React.Fragment>
        <span className="d-none d-md-block text-muted">{description}</span>
        <br />
        <div className="text-center pd-3">
          <span>
            <strong>
              Inicio: <i className="fa fa-calendar"></i>
            </strong>{" "}
            {` ${createdAt.substr(0,10)} ` }
          </span>
          <span>
            <strong>
              Fin: <i className="fa fa-calendar"></i>{" "}
            </strong>{" "}
            {createdAt.substr(0,10)} <br />
          </span>
          <Link to={`/courses/list/${id}`} className="mt-3 btn btn-primary">Ver más</Link>
        </div>
      </React.Fragment>
    );
  }

  render() {
    const { list } = this.props;
    return (
      <React.Fragment>
        {list &&
          list.map(item => (
            <Card
            key={item.id}
              title={item.name}
              img="https://picsum.photos/200/200"
              content={this.cardContent(item)}
              content_right={this.cardContentRight(item)}
            />
          ))}
      </React.Fragment>
    );
  }
}

const mapStateToProps = ({ user, course: { list } }) => ({ user, list });
const mapDispatchToProps = dispatch => ({
  updateData: (id, name, description, status) =>
    dispatch(courseActions.updateData(id, name, description, status)),
  getAll: id => dispatch(courseActions.getAll(id))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CourseDetail);
