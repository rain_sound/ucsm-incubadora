import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";

import CardInfo from "../Other/CardInfo";
import CheckGroup from "../Other/Checkbox/CheckboxGroup";
import { courseServices } from "../../services/courseServices";

const checkboxes = [
  {
    name: "checkInc",
    value: 1,
    key: "checkBox1",
    label: "Incubando"
  },
  {
    name: "checkEmJr",
    key: "checkBox2",
    value: 2,
    label: "Emprendedor Jr"
  },
  {
    name: "checkEmp",
    key: "checkBox3",
    value: 3,
    label: "Emprendedor"
  }
];

export class CreateCourse extends Component {
  constructor() {
    super();
    this.state = {
      submited: false,
      start_date: null,
      start_date: null,
      name: null,
      description: null,
      roles: {
        checkInc: false,
        checkEmJr: false,
        checkEmp: false
      }
    };
  }

  handleChange = e => {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  };
  
  handleChangeCheck = item => {
    this.setState( state => ({ roles: {
      ...state.roles,
      [item]: !state.roles[item]
    }}));
    
  };

  checked = () => {};

  handleSubmit = e => {
    e.preventDefault();
    this.setState({ submited: true });
    const { id } = this.props.user;
    const { name, description, roles, start_date, end_date } = this.state;
    if (name && description && start_date && end_date) courseServices.createCourse(id, name, description, roles, start_date, end_date);
  };

  renderUpdatePassword = () => {
    const { submited, name, description, end_date, start_date } = this.state;
    return (
      <div className="col-12">
        <div className="card">
          <div className="card-header">
            <strong>Información</strong>
          </div>
          <div className="card-body">
            <form className="form-horizontal" action="" method="post">
              <div className="form-group row">
                <div className="col-md-12">
                  <div className="input-group">
                    <div className="input-group-prepend">
                      <span className="input-group-text">
                        <i className="fa fa-user" />
                      </span>
                    </div>
                    <input
                      className="form-control"
                      onChange={this.handleChange}
                      type="text"
                      name="name"
                      id="name"
                      placeholder="Nombre del curso"
                    />
                  </div>
                  {submited && !name && (
                    <small className=" badge badge-danger">
                      Debe ingresar un nombre{" "}
                    </small>
                  )}
                </div>
              </div>
              <div className="form-group row">
                <div className="col-md-12">
                  <div className="input-group">
                    <input
                      className="form-control"
                      onChange={this.handleChange}
                      type="text"
                      name="description"
                      id="description"
                      placeholder="Descripción"
                    />

                    <div className="input-group-append">
                      <span className="input-group-text">
                        <i className="fa fa-asterisk" />
                      </span>
                    </div>
                  </div>
                  {submited && !description && (
                    <small className="badge badge-danger">
                      Debe ingresar una descripción{" "}
                    </small>
                  )}
                </div>
              </div>

              <div className="form-group´ row">
                <div className="col-md-6">
                  <div className="input-group">
                    <div className="input-group-prepend">
                      <span className="input-group-text">
                        <i className="fa fa-calendar" />
                        <strong>Inicio</strong>
                      </span>
                    </div>
                    <input
                      type="date"
                      id="start_date"
                      name="trip-start"
                      min="2019-01-01"
                      onChange={this.handleChange}
                    />
                    {submited && !start_date && (
                    <small className="badge badge-danger">
                      Debe ingresar una fecha de Inicio{" "}
                    </small>
                  )}
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="input-group">
                    <div className="input-group-prepend">
                      <span className="input-group-text">
                        <i className="fa fa-calendar" />
                        <strong>Fin</strong>
                      </span>
                    </div>
                    <input
                      type="date"
                      id="end_date"
                      name="trip-start"
                      min="2019-01-01"
                      onChange={this.handleChange}
                    />
                     {submited && !end_date && (
                    <small className="badge badge-danger">
                      Debe ingresar una fecha de Fin{" "}
                     </small>)  }
                  </div>
                </div>
              </div>
              <div className="row mt-3">
                <div className="col-12">
                  <span className="d-block">
                    <strong>Visibilidad</strong>
                  </span>
                  <CheckGroup
                    handleChange={this.handleChangeCheck}
                    checked={this.checked}
                    checkboxes={checkboxes}
                  />
                </div>
              </div>
            </form>
          </div>
          <div className="card-footer">
            <button
              onClick={this.handleSubmit}
              className="btn btn-sm btn-success"
              type="submit"
            >
              <i className="fa fa-check" /> Crear
            </button>
            <button className="btn btn-sm btn-danger" type="reset">
              <i className="fa fa-ban" /> Limpiar
            </button>
          </div>
        </div>
      </div>
    );
  };

  render() {
    return (
      <div className="row">
        <CardInfo body={this.renderUpdatePassword()} width="6" />
      </div>
    );
  }
}

const mapStateToProps = ({ user }) => ({ user });

CreateCourse.prototypes = {
  user: PropTypes.shape({
    name: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
    email: PropTypes.string.isRequired,
    imgProfile: PropTypes.string.isRequired,
    reason: PropTypes.string,
    message: PropTypes.string
  })
};

export default connect(
  mapStateToProps,
  null
)(CreateCourse);
