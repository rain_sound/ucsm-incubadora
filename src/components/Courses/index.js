import CreateCourse from './CreateCourse'
import MyCourses from './MyCourses'

export {
    CreateCourse,
    MyCourses
}