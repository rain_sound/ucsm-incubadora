import React, { Component } from "react";
import {
  Button,
  Card,
  CardBody,
  Col,
  Form,
  FormGroup,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Row
} from "reactstrap";
import { connect } from "react-redux";
import { getCourseById } from '../../reducers'
import "react-tabulator/lib/css/tabulator.min.css"; // theme
import "react-tabulator/css/bootstrap/tabulator_bootstrap.min.css"; // use Theme(s)
import "react-bootstrap-table-next/dist/react-bootstrap-table2.min.css";
import "react-bootstrap-table2-filter/dist/react-bootstrap-table2-filter.min.css";
import "react-bootstrap-table2-toolkit/dist/react-bootstrap-table2-toolkit.min.css";
import ToolkitProvider, {
  CSVExport,
  Search
} from "react-bootstrap-table2-toolkit";
import BootstrapTable from "react-bootstrap-table-next";
import { ButtonRow } from "../Other/Table";
// for React 16.4.x use: import { ReactTabulator }
import CDate from "../../helpers/CDate";
/* import { ButtonRow } from "../Other/"; */

import { courseActions } from "../../actions/course";

const time = new CDate();

class MyCourses extends Component {
  constructor(props) {
    super(props);

    this.UpdateButton = this.UpdateButton.bind(this);
    this.DeleteButton = this.DeleteButton.bind(this);
    this.state = {
      modalUpdate: false,
      modalDelete: false,
      id: null,
      name: null,
      description: null,
      status: null,
      course: null,
      courses: [],
      columns: [
        {
          dataField: "id",
          text: "Codigo",
          sort: true,
          width: 100,
          onSort: (field, order) => {
            console.log("field", field, "order", order);
          }
        },
        {
          dataField: "name",
          text: "Nombre",
          sort: true
        },
        {
          dataField: "description",
          text: "Descripción",
          sort: true
        },
        {
          dataField: "createdAt",
          text: "Fecha de creación",
          sort: true,
          formatter: (row, cell) => {
            time.setTime(row);
            return time.getFullDate();
          }
        },
        {
          dataField: "status",
          text: "Estado",
          sort: true,
          formatter: (row, cell) => (row === false ? "Cerrado" : "En curso")
        },
        {
          dataField: "actionsUpdate",
          text: "Actions",
          csvExport: false,
          isDummyField: false,
          formatter: this.UpdateButton,
          style: { textAlign: "center" }
        },
        {
          dataField: "actionsDelete",
          text: "Actions",
          isDummyField: false,
          csvExport: false,
          formatter: this.DeleteButton,
          style: { textAlign: "center" }
        }
      ]
    };
  }

  componentDidMount = () => {
    const id_url = this.props.match.params.id;
    const {
      user: { id },
      getAll,getCourseById
    } = this.props;
    getAll(id);
    this.setState({
      course: getCourseById(id_url)[0]
    });

    /*  const { getAll, list } = this.props 
    if(!list || list.length === 0) getAll() */
  };

  handleChange = e => {
    const { name, value } = e.target;
    this.setState({
      [name]: value
    });
  };

  //IMPORTANT:
  // Modal Methods
  toggleUpdate = ({ name = "", description = "", status = "", id = "" }) => {
    this.setState({
      modalUpdate: !this.state.modalUpdate,
      name,
      description,
      status,
      id
    });
  };

  toggleDelete = ({ code }) => {
    this.setState({ modalDelete: !this.state.modalDelete, code });
  };
  //Modal Methods
  //IMPORTANT:

  //IMPORTANT:
  // Update row after update click button
  // Update server
  // If update server was successful update view
  updateDataTable = e => {
    e.preventDefault();
    const { id, name, description, status } = this.state;
    const { updateData } = this.props;
    updateData(id, name, description, status);
    this.setState({ modalUpdate: false });
  };
  //IMPORTANT:
  //BUTTON COLUMN TABLE
  UpdateButton(cell, row) {
    return (
      <ButtonRow
        title="Actualizar"
        toggle={this.toggleUpdate}
        row={row}
        width={"w-75"}
        type="success"
      />
    );
  }

  //BUTTON COLUMN TABLE
  DeleteButton(cell, row) {
    return (
      <ButtonRow
        title="Borrar"
        toggle={this.toggleDelete}
        row={row}
        width={"w-75"}
        type="danger"
      />
    );
  }

  render() {
    const { list } = this.props;
    const { id, name, description, status, course } = this.state;
    const { ExportCSVButton } = CSVExport;
    const { SearchBar, ClearSearchButton } = Search;
    return (
      <React.Fragment>
        { course && <div className="row ">
          <div className="m-auto col-8 card-generic">
            <div className="row">
              <div className="col-3 text-center">
                <img className="img-fluid rounded" src="https://picsum.photos/100/100" alt="" />
              </div>
              <div className="col-6">
                <h4>{ course.name }</h4>
                <span>
                  { course.description }
                </span>
                <button className="btn btn-primary">Cerrar curso</button>
              </div>
              
            </div>
          </div>
        </div> }

        <div className="row mt-3">
          <div className="col-auto">
            {list && (
              <ToolkitProvider
                keyField={"id"}
                classes="card table-responsive"
                data={list}
                columns={this.state.columns}
                search
                loading
                exportCSV={{
                  onlyExportFiltered: true,
                  ignoreHeader: true,
                  exportAll: false
                }}
              >
                {props => (
                  <div className="card">
                    <div className="card-header">
                      <div className="row">
                        <div className="col-3">
                          <SearchBar tableId="id" {...props.searchProps} />
                        </div>
                        <div className="col-auto">
                          <ClearSearchButton
                            className="btn btn-secondary"
                            {...props.searchProps}
                          />
                        </div>
                        <div className="col-auto">
                          <ExportCSVButton
                            className="btn btn-success"
                            {...props.csvProps}
                          >
                            Export CSV
                          </ExportCSVButton>
                        </div>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-12">
                        <BootstrapTable
                          hover
                          striped
                          {...props.baseProps}
                          rowEvents={this.rowEvents}
                        />
                      </div>
                    </div>
                  </div>
                )}
              </ToolkitProvider>
            )}
          </div>

          {/* Modal Delete */}
          <Modal isOpen={this.state.modalDelete} toggle={this.toggleDelete}>
            <ModalHeader toggle={this.toggle}>Modal Delete</ModalHeader>
            <ModalBody>Este es un modal delete</ModalBody>
            <ModalFooter>
              <Button color="primary" onClick={this.toggleDelete}>
                Do Something
              </Button>
              <Button color="secondary" onClick={this.toggleDelete}>
                Cancel
              </Button>
            </ModalFooter>
          </Modal>

          {/* Modal Update */}
          <Modal isOpen={this.state.modalUpdate} toggle={this.modalUpdate}>
            <ModalHeader className="bg-info color-white" toggle={this.toggle}>
              Actualizar Informacion
            </ModalHeader>
            <ModalBody>
              <Row>
                <Col xs="12">
                  <Card>
                    <CardBody>
                      <Form action="" method="post">
                        <FormGroup>
                          <InputGroup>
                            <InputGroupAddon
                              addonType="prepend"
                              className="w-25"
                            >
                              <InputGroupText className="w-100">
                                Codigo
                              </InputGroupText>
                            </InputGroupAddon>
                            <Input
                              type="text"
                              onChange={this.handleChange}
                              name="id"
                              id="id"
                              disabled
                              value={id}
                            />
                            <InputGroupAddon addonType="append">
                              <InputGroupText>
                                <i className="fa fa-user" />
                              </InputGroupText>
                            </InputGroupAddon>
                          </InputGroup>
                        </FormGroup>
                        <FormGroup>
                          <InputGroup>
                            <InputGroupAddon
                              addonType="prepend"
                              className="w-25"
                            >
                              <InputGroupText className="w-100">
                                Nombre
                              </InputGroupText>
                            </InputGroupAddon>
                            <Input
                              type="text"
                              onChange={this.handleChange}
                              name="name"
                              disabled
                              id="name"
                              value={name}
                            />
                            <InputGroupAddon addonType="append">
                              <InputGroupText>
                                <i className="fa fa-user" />
                              </InputGroupText>
                            </InputGroupAddon>
                          </InputGroup>
                        </FormGroup>
                        <FormGroup>
                          <InputGroup>
                            <InputGroupAddon
                              addonType="prepend"
                              className="w-25"
                            >
                              <InputGroupText className="w-100">
                                Descripción
                              </InputGroupText>
                            </InputGroupAddon>
                            <Input
                              type="text"
                              onChange={this.handleChange}
                              name="description"
                              id="description"
                              value={description}
                            />
                            <InputGroupAddon addonType="append">
                              <InputGroupText>
                                <i className="fa fa-asterisk" />
                              </InputGroupText>
                            </InputGroupAddon>
                          </InputGroup>
                        </FormGroup>
                        <FormGroup>
                          <InputGroup>
                            <InputGroupAddon
                              addonType="prepend"
                              className="w-25"
                            >
                              <InputGroupText className="w-100">
                                Estado
                              </InputGroupText>
                            </InputGroupAddon>
                            <Input
                              type="text"
                              onChange={this.handleChange}
                              name="status"
                              id="status"
                              value={
                                status === false ? "En curso" : "Completado"
                              }
                            />
                            <InputGroupAddon addonType="append">
                              <InputGroupText>@</InputGroupText>
                            </InputGroupAddon>
                          </InputGroup>
                        </FormGroup>
                        <FormGroup className="form-actions text-center">
                          <Button
                            type="submit"
                            color="primary"
                            onClick={this.updateDataTable}
                          >
                            <i className="fa fa-dot-circle-o" /> Actualizar
                          </Button>
                          <Button
                            type="reset"
                            color="danger"
                            onClick={this.toggleUpdate}
                          >
                            <i className="fa fa-ban" /> Limpiar
                          </Button>
                        </FormGroup>
                      </Form>
                    </CardBody>
                  </Card>
                </Col>
              </Row>
            </ModalBody>
          </Modal>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = ({ user, course: { list }, course, getCountPosts }) => ({ user, list,
  getCourseById: id => getCourseById(course, id) });
const mapDispatchToProps = dispatch => ({
  updateData: (id, name, description, status) =>
    dispatch(courseActions.updateData(id, name, description, status)),
  getAll: id => dispatch(courseActions.getAll(id))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MyCourses);
