import React, { Component } from 'react'
import CommentCard from './CommentCard'

class CommentList extends Component {
    renderPosts(){
        return this.props.comments.map(item => <CommentCard key={item.id} params={item}></CommentCard>)
    }
    render() {
    return (
      <div>
          {this.props.comments?this.renderPosts():null}
      </div>
    )
  }
}
export default CommentList