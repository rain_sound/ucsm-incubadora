import React, {Component} from 'react'
import { Card, CardImg, CardText, CardBody, CardLink, CardTitle, CardSubtitle, Button} from 'reactstrap'

/**PARAMS
 * author_username
 * --author_photo
 * body
 * creation_date
 * owner
 */

class CommentCard extends Component {
    editable(owner){
        if(owner){
            return (
                <div>
                    <Button>Editar</Button>{'  '}
                    <Button>Eliminar</Button>
                </div>
            )
        }
    }
    render(){
        let {author_username, body, creation_date, owner, user} = this.props.params
        return (
            <div class="card border-info mb-3" >
                <div class="card-body">
                    <p class="card-text">{user.name+": "+body}</p>
                </div>
            </div>
        )
    }
}

export default CommentCard