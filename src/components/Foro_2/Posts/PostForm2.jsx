import React, {Component} from 'react'
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import {postActions} from '../../../actions/posts'
import {connect} from 'react-redux'
import {getUserId} from '../../../reducers'
import PostImage from './PostImage'
/**Parametros
 * modal = estado inical del modal
 * edit = si se abrita el formulario para actualizar
 *  head = titulo del post a modificar 
 *  body = contenido del post a modificar
 * 
 * let PostFormParams = {
            edit: true,
            head: '',
            body: '',
            id: ''
        }
 */
class PostForm2 extends Component{
    constructor() {
        super();
        this.state = {
            head: '',
            body: '',
            id: ''
        };
        this.regEntrada = this.regEntrada.bind(this)
        this.cancelar = this.cancelar.bind(this)
        this.editConfig = this.editConfig.bind(this)
    }
    handleChange = (e) => {
        const { name, value } = e.target
        this.setState({
            [name]: value
        })
    }
    regEntrada(){
        this.props.regPost(this.props.userId, this.state.head, this.state.body)
        window.location.reload();
    }
    cancelar(){
        this.setState({
            head: '',
            body: '',
        })
        this.toggle()
    }
    editConfig(){
        if(this.state.edit)
            return (
                <div>        
                    <Button color="primary" onClick={this.actualizarEntrada}>Actualizar</Button>
                </div>
            )
        else{
            return <Button color="primary" onClick={this.regEntrada}>Registrar</Button>
        }
    }
    render(){
        const { head, body } = this.state
        return (
         
            <div>
              <Form>
                <FormGroup>
                  <Label for="head_txt_entrada_form">
                    Cabecera de la entrada
                  </Label>
                  <Input
                    type="text"
                    id="head_txt_entrada_form"
                    name="head"
                    placeholder="Tema principal.."
                    value={head}
                    onChange={this.handleChange}
                  />
                </FormGroup>
                <FormGroup>
                  <Label for="body_txt_entrada_form">
                    Contenido de la entrada
                  </Label>
                  <Input
                    type="textarea"
                    id="body_txt_entrada_form"
                    name="body"
                    value={body}
                    onChange={this.handleChange}
                  />
                </FormGroup>
              </Form>
              <Button color="primary" onClick={this.regEntrada}>
                Registrar
              </Button>
              {" \n"}
              <Button color="secondary" onClick={this.cancelar}>
                Cancelar
              </Button>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    userId: getUserId(state)
})

const mapDispatchToProps = dispatch => ({
    regPost: (userId, head, body) => dispatch(postActions.registerPost(userId, head, body)),
    actualizarPost: (id, head, body) => dispatch(postActions.updatePost(id, head, body))
})

export default connect(mapStateToProps, mapDispatchToProps)(PostForm2)