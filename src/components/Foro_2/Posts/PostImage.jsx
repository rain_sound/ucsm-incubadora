import React, { Component } from "react";
import { connect } from "react-redux";
import { Form, Input, CardImg, Alert } from "reactstrap";
import { postActions } from "../../../actions/posts"
import { URL_PUBLIC } from "../../../helpers/api";
import {getImgPost, getSelectedPost} from "../../../reducers"

//IMPORTANT:
// Config Variables Component

const MAX_SIZE_IMAGE    = 2 * 1024 * 1024;
const EXT_ALLOWED_IMAGE = ["jpeg", "jpg"];

const ERROR_SIZE        = 'Archivo muy grande';
const ERROR_EXTENSION   = 'Extension no permitida';


class PostImage extends Component {
  constructor() {
    super();
    this.state = {
      error: null,
      srcImage: "",
      selectedImage: null,
      file: null
    };
    this.handleUpdate = this.handleUpdate.bind(this)
  }

  handleChangeImage = e => {
    const fileTemp = e.target.files[0];

    this.setState({
      selectedImage: fileTemp,
      file: URL.createObjectURL(fileTemp),
      error: null
    });

    //SIZE validation
    if (fileTemp.size > MAX_SIZE_IMAGE) {
      this.setState({ error: ERROR_SIZE });
    }

    //Extension validation
    var checked = false;
    EXT_ALLOWED_IMAGE.forEach(ext => {
      if (`image/${ext}` === fileTemp.type) {
        checked = true;
      }
    });
    if (!checked) this.setState({ error: ERROR_EXTENSION });
  };

  handleUpdate = e => {
    e.preventDefault();

    const { selectedImage } = this.state;
    const { id } = this.props.selectedPost;
    if (selectedImage) {
      this.props.updateImage(selectedImage, id);
      this.setState({ file: null });
    }
  };

  render() {
    const { file, error } = this.state;
    return (
      <Form onSubmit={this.handleUpdate}>
        <div className="text-center">
          <Input
            accept="image/*"
            name="image"
            type="file"
            onChange={this.handleChangeImage}
          />

          <CardImg
            top
            width="100px"
            height="auto"
            src={file ? file : `${URL_PUBLIC}${this.props.imgSrc}`}
            alt="Card image cap"
            className={
              error ? "img-validation-error" : "img-validation-success"
            }
          />
        </div>
        <Input
          className="btn btn-primary"
          disabled={error}
          type="submit"
          value="SUBIR IMAGEN"
        />
      </Form>
    );
  }
}

const mapStateToProps = ( state ) => ({
  imgSrc: getImgPost(state),
  selectedPost: getSelectedPost(state)
});

const mapDispatchToProps = dispatch => ({
  updateImage: (image, id) => dispatch(postActions.updateImage(image, id))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PostImage);
