import React, { Component } from 'react'
import { URL_PUBLIC } from "../../../helpers/api";
import {Card, CardBody, CardTitle, CardSubtitle, CardText, CardImg} from 'reactstrap'
/**PARAMS this.props.post
 * los datos de un post
 */

class PostPage extends Component {
  render() {
    let {head, body, user, createdAt, postimg} = this.props.post 
    let date = new Date(createdAt)
    let months = ['Ene', 'Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Nov','Dic']
    return (
      <div class="row no-gutters">
        <div class="col-md-2">
          <img src={`${URL_PUBLIC}${postimg}`} class="img-thumbnail" alt="Card image cap" />
        </div>
        <div class="col-md-10">
          <div class="card-body">
            <h5 class="card-title">{head}</h5>
            <p class="card-text">{body}</p>
            <p class="card-text"><small class="text-muted">by: {user.name} at: {`${months[date.getMonth()]}, ${date.getDate()}`}</small></p>
          </div>
        </div>
      </div>
    )
  }
}

export default PostPage;