import React, {Component} from 'react'
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import {postActions} from '../../../actions/posts'
import {connect} from 'react-redux'
import {getUserId} from '../../../reducers'
import PostImage from './PostImage'
import CardImage from '../../Other/CardImage';
/**Parametros
 * modal = estado inical del modal
 * edit = si se abrita el formulario para actualizar
 *  head = titulo del post a modificar 
 *  body = contenido del post a modificar
 * 
 * let PostFormParams = {
            modal: false,
            edit: true,
            head: '',
            body: '',
            id: ''
        }
 */
class PostForm extends Component{
    constructor(props) {
        super(props);
        let { modal, edit, head, body, id, post } = props.params
        this.state = {
            head,
            body,
            modal,
            edit,
            id,
            post
        };
        this.toggle = this.toggle.bind(this);
        this.regEntrada = this.regEntrada.bind(this)
        this.cancelar = this.cancelar.bind(this)
        this.editConfig = this.editConfig.bind(this)
        this.actualizarEntrada = this.actualizarEntrada.bind(this)
    }
    
    toggle() {
        // alert(this.state.post)
        // this.props.selectPost(this.state.post)
        this.setState({
            modal: !this.state.modal
        });
    }
    handleChange = (e) => {
        const { name, value } = e.target
        this.setState({
            [name]: value
        })
    }
    regEntrada(){
        this.props.regPost(this.props.userId, this.state.head, this.state.body)
        this.toggle()
        window.location.reload();
    }
    cancelar(){
        this.setState({
            head: '',
            body: '',
        })
        this.toggle()
    }
    actualizarEntrada(){
        let {id, head, body} = this.state
        this.props.actualizarPost(id, head, body)
        this.toggle()
        window.location.reload();
    }
    editConfig1(){
        if(this.state.edit)
            return <Button color="primary" onClick={this.toggle} ><i className="material-icons">edit</i></Button>
        else{
            return <Button color="link" onClick={this.toggle} >CREAR ENTRADA </Button>
        }
    }
    editConfig(){
        if(this.state.edit)
            return (
                <div>        
                    <Button color="primary" onClick={this.actualizarEntrada}>Actualizar</Button>
                </div>
            )
        else{
            return <Button color="primary" onClick={this.regEntrada}>Registrar</Button>
        }
    }
    render(){
        const { head, body } = this.state
        return (
            <div style={{float: "left", marginRight: "10px"}}>
                {this.editConfig1()}
                <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
                    <ModalHeader toggle={this.toggle}>Crear Entrada</ModalHeader>
                    <ModalBody>
                        <Form>
                            <FormGroup>
                                <Label for="head_txt_entrada_form">Cabecera de la entrada</Label>
                                <Input type="text" id="head_txt_entrada_form" name="head" placeholder="Tema principal.." value={head} onChange={ this.handleChange }/> 
                            </FormGroup>
                            <FormGroup>
                                <Label for="body_txt_entrada_form">Contenido de la entrada</Label>
                                <Input type="textarea" id="body_txt_entrada_form" name="body" value={body} onChange={ this.handleChange }/>
                            </FormGroup>
                        </Form>
                        {this.state.edit && <PostImage />}
                    </ModalBody>
                    <ModalFooter>
                        {this.editConfig()}{' \n'}
                        <Button color="secondary" onClick={this.cancelar}>Cancelar</Button>
                    </ModalFooter>
                </Modal>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    userId: getUserId(state)
})

const mapDispatchToProps = dispatch => ({
    regPost: (userId, head, body) => dispatch(postActions.registerPost(userId, head, body)),
    actualizarPost: (id, head, body) => dispatch(postActions.updatePost(id, head, body)),
    selectPost: post => dispatch(postActions.selectedPost(post))
})

export default connect(mapStateToProps, mapDispatchToProps)(PostForm)