import React, { Component } from 'react'
import PostCard from '../../../components/Foro_2/Posts/PostCard'
import PostRow from './PostRow';

class PostTable extends Component {
  renderPosts(){
        return this.props.posts.map(item => <PostRow handler={this.props.handler} key={item.id} params={item}></PostRow>)
    }
    render() {
    return (
        <table class="table">
        <thead>
          <tr>
            <th scope="col">Titulo</th>
            <th scope="col">Creado</th>
            <th scope="col">Actualizado</th>
            <th scope="col">Opciones</th>
          </tr>
        </thead>
        <tbody>
          {this.props.posts?this.renderPosts():null}
        </tbody>
      </table>
    )
  }
}
export default PostTable