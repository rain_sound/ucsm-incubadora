import React, {Component} from 'react'
import { Card, CardBody, CardTitle, CardSubtitle} from 'reactstrap'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { postActions } from '../../../actions/posts'
import { URL_PUBLIC } from "../../../helpers/api";
import PostForm from './PostForm'
/**Parametros
 * id
 * author_username
 * ---author-photo
 * head
 * body
 * createdAt
 */

class PostRow extends Component {
    constructor(props){
        super(props)
        this.clickEntry = this.clickEntry.bind(this)
        this.deletePost = this.deletePost.bind(this)
    }
    renderButtons(head, body, id, post){
        let modaleditparams = {
          modal: false, 
          edit: true,
          head,
          body,
          id,
          post
        }
        return (
          <div> 
            <PostForm params={modaleditparams}>Editar</PostForm>
            <button className="btn btn-primary" onClick={this.deletePost}><i className="material-icons">delete</i></button>        
          </div>
        )
    }
    deletePost(){
        this.props.deletePost(this.props.params.id)
        this.props.handler()
    }
    clickEntry(){
        let {id} = this.props.params
        this.props.selectPost(this.props.params)
        this.props.history.push(`/post/${id}`)
    }
    render(){
        let {user, head, createdAt, updatedAt, body, id} = this.props.params
        
        let date1 = new Date(createdAt)
        let date2 = new Date(updatedAt)
        let months = ['Ene', 'Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Nov','Dic']
        return (
            <tr>
                <td onClick={this.clickEntry}>{head}</td>
                <td>{`${date1.getDate()}/${months[date1.getMonth()]}/${date1.getFullYear()}`}</td>
                <td>{`${date2.getDate()}/${months[date2.getMonth()]}/${date2.getFullYear()}`}</td>
                <td>{this.renderButtons(head, body, id, this.props.params)}</td>
            </tr>
            
        )
    }

}

const mapDispatchToProps = dispatch => ({
    selectPost: post => dispatch(postActions.selectedPost(post)),
    deletePost: (id) => (dispatch(postActions.deletePost(id))),
})

export default withRouter(connect(null, mapDispatchToProps)(PostRow));