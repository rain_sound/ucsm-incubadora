import React, {Component} from 'react'
import { Card, CardBody, CardTitle, CardSubtitle} from 'reactstrap'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { postActions } from '../../../actions/posts'
import { URL_PUBLIC } from "../../../helpers/api";

/**Parametros
 * id
 * author_username
 * ---author-photo
 * head
 * body
 * createdAt
 */

class PostCard extends Component {
    constructor(props){
        super(props)
        this.clickEntry = this.clickEntry.bind(this)
    }
    clickEntry(){
        let {id} = this.props.params
        this.props.selectPost(this.props.params)
        this.props.history.push(`/post/${id}`)
    }
    render(){
        let {user, head, createdAt, postimg, body} = this.props.params
        
        let date = new Date(createdAt)
        let months = ['Ene', 'Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Nov','Dic']
        return (
            // <div className="container">
            // <br/>
            // <Card>
            //     <CardBody>
            //         <CardTitle onClick={this.clickEntry}>{head}</CardTitle>
            //         <CardSubtitle>{"by: "+user.name}</CardSubtitle>
            //         <CardSubtitle>{createdAt}</CardSubtitle>
            //     </CardBody>
            // </Card>
            <div class="card text-white bg-secondary mb-3" style={{maxWidth: "90%"}} onClick={this.clickEntry}>
                <div class="row no-gutters">
                    <div class="col-md-2 card-header">
                        <img src={`${URL_PUBLIC}${user.imgProfile}`} alt="Card image cap" style={{borderRadius: "30%", padding: "5px", width: "60%", display: "block", marginLeft: "auto", marginRight: "auto"}} />
                        <p class="card-text" style={{paddingLeft: "15px"}}><small >by: {user.name} </small></p>
                        <p class="card-text" style={{paddingLeft: "15px"}}><small>{`${months[date.getMonth()]}, ${date.getDate()}`}</small></p>
                    </div>
                    <div class="col-md-10">
                    <div class="card-body">
                        <h3 class="card-title">{head}</h3>
                        <p class="card-text">{body}</p>
                    </div>
                    </div>
                </div>
            </div>
        )
    }

}

const mapDispatchToProps = dispatch => ({
    selectPost: post => dispatch(postActions.selectedPost(post))
})

export default withRouter(connect(null, mapDispatchToProps)(PostCard));