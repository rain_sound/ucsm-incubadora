import React, { Component } from 'react'
import PostCard from '../../../components/Foro_2/Posts/PostCard'

class PostList extends Component {
    renderPosts(){
        return this.props.posts.map(item => <PostCard key={item.id} params={item}></PostCard>)
    }
    render() {
    return (
      <div>
          {this.props.posts?this.renderPosts():null}
      </div>
    )
  }
}
export default PostList