import React, {Component} from 'react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink} from 'reactstrap';
import PostForm from '../Posts/PostForm'


class NavBar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpen: false,
            loged: props.loged
        };
        
        this.toggle = this.toggle.bind(this);
    }
    PostFormParams = {
        modal: false,
        edit: false,
        head: '',
        body: '',
        id: ''
    }
    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }
    render() {
        return (
            <div>
                <Navbar color="light" light expand="md">
                    <NavbarBrand href="/foro">FORO</NavbarBrand>
                    <NavbarToggler onClick={this.toggle} />
                    <Collapse isOpen={this.state.isOpen} navbar>
                    <Nav className="ml-auto" navbar>
                        <NavItem>
                        <NavLink href="#">Perfil</NavLink>
                        </NavItem>
                        <NavItem>
                        <NavLink href="/foro/myposts">Mis Entradas</NavLink>
                        </NavItem>
                        <NavItem>
                            <PostForm params={this.PostFormParams} />
                        </NavItem>
                    </Nav>
                    </Collapse>
                </Navbar>
            </div>
        );
    }
}


export default NavBar;