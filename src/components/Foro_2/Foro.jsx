import React, {Component} from 'react'
import {connect} from 'react-redux'
import {Route, Switch} from 'react-router-dom'
import PostsListContainer from '../../containers/Foro_2/Posts/PostsListContainer'
import PostPageContainer from '../../containers/Foro_2/Posts/PostPageContainer'
import {pagesConstants} from '../../constants/entrada/foroPages'
import PostTableContainer from '../../containers/Foro_2/Posts/PostTableContainer';

class Foro extends Component{
    constructor(props) {
        super(props)
        this.state = {
            toggle: false
        }
    }

    renderAlert = () => {
        console.log('entro aqui')
        const { toggle } = this.state
        const { user } = this.props
        console.log('proos', this.props, 'rol', user.rol, 'type', typeof(user.rol), 'accept', user.accept)
        if((user.rol === '2' || user.rol === '3') && user.accept === false ){
            console.log('entro auqi')
            return (
                <div
                  class={`alert alert-warning alert-dismissible fade show ${
                    toggle ? "d-none" : ""
                  }`}
                  role="alert"
                >
                  <strong>Tu registro está casi completo!!</strong> Un administrador
                  debe aprobar tu registro para que seas considerado
                  mentor/emprendedor Jr, se te enviará un mensaje al correo que
                  proporcionaste con la respuesta.
                  <button
                    type="button"
                    class="close"
                    data-dismiss="alert"
                    aria-label="Close"
                    onClick={() => this.setState({ toggle: true })}
                  >
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
              );
        }
        
    }
    getAllPosts = () => (<div><PostsListContainer type={pagesConstants.ALL_POSTS}></PostsListContainer></div>)
    getMyPosts = () => (<div><PostTableContainer type={pagesConstants.MY_POSTS}></PostTableContainer></div>)
    getPostPage = () => (<div><PostPageContainer></PostPageContainer></div>)
    render(){
        
        
        return (
          <div className="row" style={{ height: "100%" }}>
            {/*MainContent*/}
            <div className="col" style={{ height: "100%" }}>
              {/* <PostCard params={ParamsPostCard} />
                    <CommentCard params={ParamsCommentCard}></CommentCard> */}
                    <Switch>
                        <Route path='/myposts/:id' component={this.getMyPosts}></Route>
                        <Route path='/foro/:id' component={this.getAllPosts}></Route>
                    </Switch>
                    <br></br>
                    {/* <Entrada params={{user_id: 1, body: "cuerpo", head: "Cabeza", id: 1}}></Entrada> */}
                </div>
            </div>
        );
    }
}


const mapStateToProps = ( {foro_mp, data, user} ) => ({foro_mp, data, user})



export default connect(mapStateToProps, null)(Foro);
