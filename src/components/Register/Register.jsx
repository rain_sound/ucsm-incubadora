import React from 'react'

import { userService } from '../../services/userServices'

import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { withRouter } from 'react-router-dom'

class Register extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            username: '',
            password: '',
            confirmPassword: '',
            email: '',
            submited: false,
        }

    }

    toggle = ()  => {
        this.setState(prevState => ({
          modal: !prevState.modal
        }));
      }

    handleSubmit = (e) => {
        e.preventDefault()
        this.setState({ submited: true, modal: false })
        const { username, password, confirmPassword, email } = this.state

        if(username && password && email && (password === confirmPassword)) {
            userService.register(username, password, email)
        }
    }

    handleChange = (e) => {
        const { name, value } = e.target
        this.setState({
            [name]: value
        })
    }


    render() {
        const { username, password, confirmPassword, email, submited } = this.state
        return(
            <div className="container">
                <div className="row">
                 <div className="col-sm-9 col-md-7 col-lg-5 mx-auto">
                    <div className="card card-signin my-5">
                        <div className="card-body">
                            <h5 className="card-title text-center">Registro</h5>
                            <form onSubmit={ this.handleSubmit }>
                                <input className="form-control" placeholder="Ingrese un username" type="text" name="username" value={ username } onChange={ this.handleChange }/>
                                { submited && !username && <small className="text--error"> Debe de ingresar un username </small>}
                                <input className="form-control mt-3" placeholder="Ingrese una contrasenia" type="password" name="password" value={ password } onChange={ this.handleChange }/>
                                { submited && !password && <small className="text--error"> Debe de ingresar un contrasenia </small>}
                                <input className="form-control mt-3" placeholder="Repita la contrasenia " type="password" name="confirmPassword" value={ confirmPassword } onChange={ this.handleChange }/>
                                { submited && !(confirmPassword === password) && <small className="text--error"> Las contrasenias deben de ser iguales </small>}
                                <input className="form-control mt-3" placeholder="Ingrese un correo" type="text" name="email" value={ email } onChange={ this.handleChange }/>
                                { submited && !email && <small className="text--error"> Debe de ingresar un correo </small>}
                                <div className="btn-center mt-3 mb-3">
                                    <input type="submit" className="btn btn-primary" onSubmit={ this.handleSubmit } value="Registrar"/>
                                </div>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        )
    }
}

export default withRouter(Register)