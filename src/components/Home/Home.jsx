import React from "react";
import { connect } from "react-redux";
import { userActions } from "../../actions/user"

class Home extends React.Component {
  constructor(props) {
    super(props);
    this.resetUser();
  }

  resetUser = () => {
    this.props.logout();
  };

  render() {
    return (
      <div className="container">
      </div>
    );

  }
}

const mapDispatchToProps = dispatch => ({
  logout: () => dispatch(userActions.logout())
});

export default connect(null, mapDispatchToProps)(Home);
