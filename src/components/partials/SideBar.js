import React from 'react';
import { connect } from 'react-redux'
import { URL_PUBLIC } from "../../helpers/api";
import DropDown from '../Other/DropDown';
import { MdForum, MdImportContacts, MdCreate } from 'react-icons/md'

class Sidebar extends React.Component {
    constructor(props){
        super(props);
    }

  showSettings (e) {
    e.preventDefault();
  }

  profileDD = () => {
      return (
        <React.Fragment>
          <li className="nav-item dropdown__item active">
            <a className="nav-link" href={`/profile/${this.props.user.name}`}>
              <span>Configuración</span>
            </a>
          </li>
          <li className="nav-item dropdown__item active">
          <a className="nav-link" href={`/profile/${this.props.user.name}/pass`}>
              <i className="fas fa-fw fa-tachometer-alt"></i>
              <span>Credenciales</span>
            </a>
          </li>
        </React.Fragment>
        )
  }

  render () {
    const { user } = this.props;
    return (
      <div className="sidebar-bg">
        <ul className="sidebar navbar-nav">
        <div className=" text-white text-center mt-2">
          <img className="align-baseline img-fluid rounded w-25" src={`${URL_PUBLIC}${user.profile.filename}`}/>
          <div className="d-inline-block m-3">
            <span> <strong>{ user.name }</strong></span>
            <small className="d-block font-weight-light">Administrator</small>
            <div className="dot"></div><span className="font-weight-light">Online</span>
          </div>
        </div>
        <hr className="separator"></hr>
        <DropDown name="Perfil" children={this.profileDD()} />
        <li className="nav-item active">
          <a className="nav-link" href="/">
            <MdCreate className="sidebar__icon"/>
            <span>Foro</span>
          </a>
        </li>
        <li className="nav-item">
          <a className="nav-link" href="/">
          <MdForum className="sidebar__icon"/>
            <span>Perfil</span></a>
        </li>
        <li className="nav-item">
          <a className="nav-link" href="/">
          <MdImportContacts className="sidebar__icon"/>
            <span>Otros</span></a>
        </li>
      </ul>
      </div>
    );
  }
}

const mapStateToProps = ({ user }) => ({ user })
export default connect(mapStateToProps, null)(Sidebar);