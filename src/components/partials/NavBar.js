import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle
} from "reactstrap";
import Login from "../Login"
import PostForm from "../../components/Foro_2/Posts/PostForm";

class NavBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
      loged: props.loged,
      dropdownOpen: false
    };

    this.toggle = this.toggle.bind(this);
  }
  PostFormParams = {
    modal: false,
    edit: false,
    head: "",
    body: "",
    id: ""
  };
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  isLogged = () => {
    return (
      <React.Fragment>
        <NavItem>
          <PostForm params={this.PostFormParams} />
        </NavItem>
        <UncontrolledDropdown
          setActiveFromChild
          className="dropdown-responsive"
        >
          <DropdownToggle tag="a" className="nav-link" caret>
            Mi Cuenta
          </DropdownToggle>
          <DropdownMenu right>
            <DropdownItem tag="a" href="/foro/myposts">
              Mis Entradas
            </DropdownItem>
            <DropdownItem tag="a" href="/">
              Cerrar Sesion
            </DropdownItem>
          </DropdownMenu>
        </UncontrolledDropdown>
      </React.Fragment>
    );
  };

  isGuest = () => {
    return (
      <Nav pills>
        <NavItem className="mr-2">
          <Login/>
        </NavItem>
        <NavItem>
          <NavLink className="btn btn-success text-white" style={{ padding: '6px 12px' }}href="/register">Registrarse</NavLink>
        </NavItem>
      </Nav>
    );
  };

  render() {
    const { user } = this.props;
    return (
      <div>
        <Navbar className="navbar--default" light expand="md">
          <NavbarBrand href="/foro">INNICIA</NavbarBrand>
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="ml-auto" navbar>
              {user.id ? this.isLogged() : this.isGuest()}
            </Nav>
          </Collapse>
        </Navbar>
      </div>
    );
  }
}


const mapStateToProps = ({ user }) => ({ user });

NavBar.propTypes = {
  user: PropTypes.object
};

export default connect(
  mapStateToProps,
  null
)(NavBar);
