import React from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { Button, Modal, ModalHeader, ModalBody } from "reactstrap";

import { userActions } from "../../actions/user";

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      submited: false,
      modal: false
    };
  }

  toggle = () => {
    this.setState({
      modal: !this.state.modal
    });
  };

  handleChange = e => {
    const { name, value } = e.target;
    this.setState({
      [name]: value
    });
  };

  handleSubmit = e => {
    e.preventDefault();
    this.setState({ submited: true });
    const { username, password } = this.state;
    const { login, history } = this.props;
    if (username && password)
      login(username, password, history)
  };

  render() {
    const { username, password, submited } = this.state;
    return (
      <div>
        <Button color="primary" onClick={this.toggle}>
          Iniciar Sesion
        </Button>
        <Modal isOpen={this.state.modal} toggle={this.toggle}>
          <ModalHeader toggle={this.toggle}>INNICIA</ModalHeader>
          <ModalBody>
            <form onSubmit={this.handleSubmit} className="">
              <input
                name="username"
                value={username}
                onChange={this.handleChange}
                className="form-control"
                type="text"
                placeholder="Ingrese el nombre de usuario"
              />
              {!username && submited && (
                <small className="text--error">
                  Debe de ingresar un nombre{" "}
                </small>
              )}
              <input
                name="password"
                value={password}
                onChange={this.handleChange}
                className="form-control mt-3"
                type="password"
                placeholder="Ingrese la contrasenia"
              />
              {!password && submited && (
                <small className="text--error">
                  Debe de ingresar su contrasenia
                </small>
              )}
              <div className="mt-3 col-md-12 text-center">
                <input
                  className="btn btn-primary w-50"
                  value="Ingresar"
                  type="submit"
                />
              </div>
              <div className="mt-3 col-md-12 text-center">
                <Link
                  onClick={() => {
                    this.toggle();
                  }}
                  to="/resetpassword"
                  className="ml-2 btn btn-outline-primary w-50"
                >
                  Recuperar contrasenia
                </Link>
              </div>
            </form>
          </ModalBody>
        </Modal>
      </div>
    );
  }
}


const mapDispatchToProps = dispatch => ({
  login: (username, password, history) =>
    dispatch(userActions.login(username, password, history))
});

export default withRouter(
  connect(
    null,
    mapDispatchToProps
  )(Login)
);
