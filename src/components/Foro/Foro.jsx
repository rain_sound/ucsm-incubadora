import React, {Component} from 'react';
import Navegacion from './Navegacion';
import ListaEntradas from './ListaEntradas';
import {connect} from 'react-redux'
import {mainPageConstants} from '../../constants/entrada'
import ListaEntradasContainer from '../../containers/LisaEntradasContainer'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom' 
import EntradaContainer from '../../containers/EntradaContainer';

class Foro extends Component{
    constructor(props){
        super(props)
        this.renderAnEntrie = this.renderAnEntrie.bind(this)
    }

    renderMisEntradas = () => <div><ListaEntradasContainer type={mainPageConstants.MY_ENTRIES} /></div>
    renderAllEntries = () => <div><ListaEntradasContainer type={mainPageConstants.ALL_ENTRIES} /></div>
    renderAnEntrie = () => <div><EntradaContainer></EntradaContainer></div>
    render(){
        return(
            <div style={{"height": "100%"}}>
                <div className="sticky-top">
                    <Navegacion />
                </div>
                {/** Contenido principal */}
                <div style={{"height": "100%"}}>
                    <div className="row" style={{"height": "100%"}}>
                        {/*Side Navigation*/}
                        <div className="col-3" style={{"background-color": "blue", "height":"100%"}}>
                            Navegacion lateral
                        </div>
                        {/*MainContent*/}
                        <div className="col" style={{"background-color": "red", "height":"100%"}}>
                            <Switch>
                                <Route exact path="/foro/misEntradas" component={this.renderMisEntradas}></Route>
                                <Route exact path="/foro/entrie" component={this.renderAnEntrie}></Route>
                                <Route exact path="/foro" component={this.renderAllEntries}></Route>
                            </Switch>
                            {/* <Entrada params={{user_id: 1, body: "cuerpo", head: "Cabeza", id: 1}}></Entrada> */}
                        </div>
                    </div>
                </div>
                {/* Footer */}
                <footer className="footer mt-auto py-3  bg-light">
                    <div className="container ">
                        Footer de la pagina
                    </div>
                </footer>
            </div>

        )
    }
}

export default Foro;
