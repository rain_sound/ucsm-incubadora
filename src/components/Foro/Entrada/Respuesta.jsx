import React from 'react'
import { Card, CardImg, CardText, CardBody, CardLink, CardTitle, CardSubtitle} from 'reactstrap'

const Respuesta = ({params: {user_id, head, body}}) => {
    return (
        <div>
        <Card>
            <CardBody>
            <CardSubtitle>{`user_id: ${user_id}`}</CardSubtitle>
            </CardBody>
            <CardBody>
            <CardText>{body}</CardText>
            </CardBody>
        </Card>
        </div>
    )
}

export default Respuesta;