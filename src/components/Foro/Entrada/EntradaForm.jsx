import React, {Component} from 'react'
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import {crearEntrada, actualizarEntrada} from '../../../services/entradaServices'


class EntradaForm extends Component{
    constructor(props) {
        super(props);
        let head = (props.params === undefined) ? '' : props.params.head
        let body = (props.params === undefined) ? '' : props.params.body
        let id = (props.params ===  undefined) ? '' : props.params.id
        this.state = {
            modal: props.modal,
            head,
            body,
            id,
            edit: props.modal
        };
        this.toggle = this.toggle.bind(this);
        this.regEntrada = this.regEntrada.bind(this)
        this.cancelar = this.cancelar.bind(this)
        this.editConfig = this.editConfig.bind(this)
        this.actualizarEntrada = this.actualizarEntrada.bind(this)
    }
    
    toggle() {
        this.setState({
            modal: !this.state.modal
        });
    }
    handleChange = (e) => {
        const { name, value } = e.target
        this.setState({
            [name]: value
        })
    }
    regEntrada(){
        let  {head, body} = this.state;
        crearEntrada(head, body, '1')
        this.toggle()
        window.location.reload();
    }
    cancelar(){
        this.setState({
            head: '',
            body: '',
        })
        this.toggle()
    }
    actualizarEntrada(){
        let  {id, head, body} = this.state;
        debugger
        actualizarEntrada(id ,head, body)
        this.toggle()
        window.location.reload();
    }
    editConfig(){
        if(this.state.edit)
            return <Button color="primary" onClick={this.actualizarEntrada}>Actualizar</Button>
        else{
            return <Button color="primary" onClick={this.regEntrada}>Registrar</Button>
        }
    }
    render(){
        const { head, body } = this.state
        return (
            <div>
                <Button color="link" onClick={this.toggle} >CREAR ENTRADA </Button>
                <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
                    <ModalHeader toggle={this.toggle}>Crear Entrada</ModalHeader>
                    <ModalBody>
                        <Form>
                            <FormGroup>
                                <Label for="head_txt_entrada_form">Cabecera de la entrada</Label>
                                <Input type="text" id="head_txt_entrada_form" name="head" placeholder="Tema principal.." value={head} onChange={ this.handleChange }/> 
                            </FormGroup>
                            <FormGroup>
                                <Label for="body_txt_entrada_form">Contenido de la entrada</Label>
                                <Input type="textarea" id="body_txt_entrada_form" name="body" value={body} onChange={ this.handleChange }/>
                            </FormGroup>
                        </Form>
                    </ModalBody>
                    <ModalFooter>
                        {this.editConfig()}{' '}
                        <Button color="secondary" onClick={this.cancelar}>Cancelar</Button>
                    </ModalFooter>
                </Modal>
            </div>
        )
    }
}

export default EntradaForm