import React, {Component} from 'react';
import Respuesta from './Respuesta'
const ForoService = require('../../../services/entradaServices');

class ListaRespuestas extends Component {
    constructor(props){
        super(props)
        this.state = {
            resp_id: props.resp_id,
            entradas: null
        }
    }
    componentDidMount() {
        ForoService.listarRespuestas(this.state.resp_id)
        .then(resp => {
            this.setState({
                entradas: resp
            })
        })
        .catch(err => console.log(err))
    }
    createEntries = (entradas) => {
        return entradas.map(item => <Respuesta key = {item} params={ item }></Respuesta>)
        // return entradas.map(item => <Entrada key = {item} params={ item }></Entrada>)
    }
    render(){
        return(
            <div>
                {    (this.state.entradas)? this.createEntries(this.state.entradas): null }
            </div>
        )
    }
}

export default ListaRespuestas;