import React, { Component } from 'react'
import { Card, CardImg, CardText, CardBody, CardLink, CardTitle, CardSubtitle, Button} from 'reactstrap'
import ListaRespuestas from './ListaRespuestas'
import RespuestaForm from './RespuestaForm'
import EntradaForm from './EntradaForm'


class Entrada extends Component{
    constructor(props){
        super(props)
        this.toogle = this.toogle.bind(this)
        this.editClick = this.editClick.bind(this)
        this.state={
            edit: false
        }
    }
    editClick(){
        this.setState({
            edit: !this.state.edit
        })
    }
    toogle(){
        if (this.state.edit){
            return <EntradaForm modal={true} params={this.props.params} />
        }
        else{
            return false
        }
    }
    render(){
        let {head, body, user_id, id} = this.props.params
        return (
            <div>
                <div>
                    <Card>
                        <CardBody>
                            <CardTitle>{head}</CardTitle>
                            <CardSubtitle>{user_id}</CardSubtitle>
                        </CardBody>
                        <CardBody>
                            <CardText>{body}</CardText>
                            <Button onClick={this.editClick}>Editar</Button>
                        </CardBody>
                    </Card>
                </div>
                <br/>
                <ListaRespuestas resp_id={id}></ListaRespuestas>
                <RespuestaForm resp_id={id}></RespuestaForm>
                {this.toogle()}
            </div>
        )
    }
}
    

export default Entrada;