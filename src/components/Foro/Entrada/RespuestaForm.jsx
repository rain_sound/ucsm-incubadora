import React, {Component} from 'react'
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
const ForoServices = require('../../../services/entradaServices')

class EntradaForm extends Component{
    constructor(props){
        super(props)
        this.state = {
            resp_id: props.resp_id,
            body: ""
        }
        this.registrarRespuesta = this.registrarRespuesta.bind(this)
    }

    handleChange = (e) => {
        const { name, value } = e.target
        this.setState({
            [name]: value
        })
    }

    registrarRespuesta(){
        let { resp_id, body } = this.state
        ForoServices.ingresarRespuesta(resp_id, body, "1")
        window.location.reload()
    }

    render(){
        const {body} = this.state
        return (
            <Form>
                <FormGroup>
                    <Label for="body_txt_entrada_form">Agregar una respuesta</Label>
                    <Input type="textarea" id="body_txt_entrada_form" placeholder="Tema principal.." name="body" value={body} onChange={ this.handleChange } />
                </FormGroup>
                <Button color="primary" onClick={this.registrarRespuesta}>Enviar</Button>
            </Form>
        )
    }
}

export default EntradaForm