import React, {Component} from 'react'
import { Card, CardImg, CardText, CardBody, CardLink, CardTitle, CardSubtitle, Button} from 'reactstrap'
import {connect} from 'react-redux'
import {entradaActions} from '../../../actions/entrada'
import {mainPageConstants} from '../../../constants/entrada'
import { withRouter } from 'react-router-dom'
import EntradaForm from './RespuestaForm';

class EntradaResum extends Component {
    constructor(props){
        super(props)
        this.clickEntry = this.clickEntry.bind(this)
    }
    clickEntry(){
        let data = this.props.params
        let ir_a = 'foro/entrie'
        if(this.props.foro_mp === mainPageConstants.MY_ENTRIES)
        {
            ir_a = 'entrie'
        }
        this.props.setMainPage(mainPageConstants.AN_ENTRY, data)
        this.props.history.push(ir_a)
    }
    render(){
        let {user_id, head} = this.props.params
        return (
            <div className="container" onClick={this.clickEntry}>
            <br/>
            <Card>
                <CardBody>
                    <CardTitle>{head}</CardTitle>
                    <CardSubtitle>{user_id + ": usuario_id"}</CardSubtitle>
                </CardBody>
            </Card>
            </div>
        )
    }

}


const mapStateToProps = state => ({
    foro_mp: state.mPage.foro_mp
})
const mapDispatchToProps = dispatch => ({
    setMainPage: (mainPage, data) => dispatch(entradaActions.setMainPage(mainPage, data))
})

// export default withRouter(EntradaResum)
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(EntradaResum));