import React, {Component} from 'react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
Button } from 'reactstrap';
import EntradaForm from './Entrada/EntradaForm'
import {connect} from 'react-redux'
import {entradaActions} from '../../actions/entrada'
import {mainPageConstants} from '../../constants/entrada'
import { withRouter } from 'react-router-dom'


class Navegacion extends Component {
    constructor(props) {
        super(props);
        this.toggle = this.toggle.bind(this);
        this.state = {
            isOpen: false,
        };
        this.clickNav = this.clickNav.bind(this)
    }
    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    clickNav(e){
        const {name} = e.target
        this.props.setMainPage(name, null)
        switch(name){
            case mainPageConstants.ALL_ENTRIES:
            {
                this.props.history.push('/foro')
                break
            }
            case mainPageConstants.MY_ENTRIES:
            {
                // debugger
                // if(this.props.user === 'empty'){
                //     this.props.history.replace('/')
                //     break
                // }
                this.props.history.push('/foro/misEntradas')
                break
            }
        }
    }
   
    render() {
        return (
            <div>
                <Navbar color="light" light expand="md">
                    <NavbarBrand href="#" name={mainPageConstants.ALL_ENTRIES} onClick={this.clickNav}>FORO</NavbarBrand>
                    <NavbarToggler onClick={this.toggle} />
                    <Collapse isOpen={this.state.isOpen} navbar>
                    <Nav className="ml-auto" navbar>
                        <NavItem>
                        <NavLink href="#">Perfil</NavLink>
                        </NavItem>
                        <NavItem>
                        <NavLink href="#"  name={mainPageConstants.MY_ENTRIES} onClick={this.clickNav}>Mis Entradas</NavLink>
                        </NavItem>
                        <NavItem>
                            <EntradaForm modal={false} />
                        </NavItem>
                        {/* <UncontrolledDropdown nav inNavbar>
                        <DropdownToggle nav caret>
                            Registrar
                        </DropdownToggle>
                        <DropdownMenu right>
                            <DropdownItem>
                            Entrada
                            </DropdownItem>
                            <DropdownItem>
                            Option 2
                            </DropdownItem>
                            <DropdownItem divider />
                            <DropdownItem>
                            Reset
                            </DropdownItem>
                        </DropdownMenu>
                        </UncontrolledDropdown> */}
                    </Nav>
                    </Collapse>
                </Navbar>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    user: state.user && state.user
})

const mapDispatchToProps = dispatch => ({
    setMainPage: (mainPage, data) => dispatch(entradaActions.setMainPage(mainPage, data))
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Navegacion));