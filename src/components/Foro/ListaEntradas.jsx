import React, {Component} from 'react';
import EntradaResum from './Entrada/EntradaResum'
const ForoService = require('../../services/entradaServices');



class ListaEntradas extends Component {
    createEntries = () => {
        return this.props.entries.map(item => <EntradaResum key = {item} params={ item }></EntradaResum>)
    }
    render(){
        return(
            <div>
                {    (this.props.entries)? this.createEntries(): null }
            </div>
        )
    }
}

export default ListaEntradas;