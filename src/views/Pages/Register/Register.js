import React, { Component } from "react";
import { userService } from '../../../services/userService'
import ToastList from '../../Notifications/Toasts/ToastList'
import TabList from '../../../components/Other/Tabs/TabList'

class Register extends Component {
  constructor(props) {
    super(props);

    this.state = {
      username: "",
      password: "",
      confirmPassword: "",
      email: "",
      orcid: "",
      dina: "",
      dni: "",
      cellphone: "",
      idRole: 1,
      submited: false
    };
  }
  handleSubmit = e => {
    e.preventDefault();
    console.log('state', this.state)
    this.setState({ submited: true, modal: false });
    const { username, password, confirmPassword, email, idRole, orcid, dina, dni, cellphone } = this.state;

    if (username && password && email && password === confirmPassword, idRole, orcid, dina, dni, cellphone) {
      console.log('ingreso a donde no deberia')
      userService.register(username, password, email, idRole, orcid, dina, dni, cellphone).then(res => {
        if(!res.error) this.props.history.replace('/login')
      });
    }
  };

  handleChange = e => {
    const { name, value } = e.target;
    this.setState({
      [name]: value
    });
  };

  render() {
    const { orcid, dina, dni, cellphone, username, password, confirmPassword, email, submited } = this.state
    return (
      <div className="app flex-row align-items-center">
        <div className="container">
          <ToastList />

          <div className="row justify-content-center">
            <div className="col-12 col-lg-6">
              <div className="card mx-4">
                <div className="card-body p-4">
                  <div className="text-center">
                    <h1>Registro INNICIA</h1>
                    <p className="text-muted">Crea tu cuenta</p>
                  </div>

                  <TabList >
                    <div label="Incubando" className="mt-5 tab-content">
                      <div className="input-group mb-3">
                        <div className="input-group-prepend">
                          <span className="input-group-text">
                            <i className="icon-user" />
                          </span>
                        </div>
                        <input
                          className="form-control"
                          placeholder="Ingrese un username"
                          type="text"
                          name="username"
                          value={username}
                          onChange={this.handleChange}
                        />
                      </div>
                      {submited && !username && (
                        <small className="badge badge-danger">
                          {" "}
                          Debe de ingresar un username{" "}
                        </small>
                      )}
                      <div className="input-group mb-3">
                        <div className="input-group-prepend">
                          <span className="input-group-text">
                            <i className="icon-lock" />
                          </span>
                        </div>
                        <input
                          className="form-control"
                          placeholder="Ingrese una contraseña"
                          type="password"
                          name="password"
                          value={password}
                          onChange={this.handleChange}
                        />
                      </div>
                      {submited && !password && (
                        <small className="badge badge-danger">
                          {" "}
                          Debe de ingresar un contraseña{" "}
                        </small>
                      )}
                      <div className="input-group mb-3">
                        <div className="input-group-prepend">
                          <span className="input-group-text">
                            <i className="icon-lock" />
                          </span>
                        </div>
                        <input
                          className="form-control"
                          placeholder="Repita la contraseña "
                          type="password"
                          name="confirmPassword"
                          value={confirmPassword}
                          onChange={this.handleChange}
                        />
                      </div>
                      {submited && !(confirmPassword === password) && (
                        <small className="badge badge-danger">
                          {" "}
                          Las contraseñas deben de ser iguales{" "}
                        </small>
                      )}
                      <div className="input-group mb-4">
                        <div className="input-group-prepend">
                          <span className="input-group-text">@</span>
                        </div>
                        <input
                          className="form-control"
                          placeholder="Ingrese un correo"
                          type="text"
                          name="email"
                          value={email}
                          onChange={this.handleChange}
                        />
                      </div>
                      {submited && !email && (
                        <small className="badge badge-danger">
                          {" "}
                          Debe de ingresar un correo{" "}
                        </small>
                      )}
                      <button
                        onClick={this.handleSubmit}
                        className="btn btn-block btn-success"
                        type="button"
                      >
                        Crear cuenta
                      </button>
                    </div>

                    <div
                      label="Emprendedor/Emprendedor Jr"
                    >

                   
                      <div className="input-group mb-3">
                        <div className="input-group-prepend">
                          <span className="input-group-text">
                            <i className="icon-user" />
                          </span>
                        </div>
                        <input
                          className="form-control"
                          placeholder="Ingrese un username"
                          type="text"
                          name="username"
                          value={username}
                          onChange={this.handleChange}
                        />
                      </div>
                      {submited && !username && (
                        <small className="badge badge-danger">
                          {" "}
                          Debe de ingresar un username{" "}
                        </small>
                      )}
                      <div className="input-group mb-3">
                        <div className="input-group-prepend">
                          <span className="input-group-text">
                            <i className="icon-lock" />
                          </span>
                        </div>
                        <input
                          className="form-control"
                          placeholder="Ingrese una contraseña"
                          type="password"
                          name="password"
                          value={password}
                          onChange={this.handleChange}
                        />
                      </div>
                      {submited && !password && (
                        <small className="badge badge-danger">
                          {" "}
                          Debe de ingresar un contraseña{" "}
                        </small>
                      )}
                      <div className="input-group mb-3">
                        <div className="input-group-prepend">
                          <span className="input-group-text">
                            <i className="icon-lock" />
                          </span>
                        </div>
                        <input
                          className="form-control"
                          placeholder="Repita la contraseña "
                          type="password"
                          name="confirmPassword"
                          value={confirmPassword}
                          onChange={this.handleChange}
                        />
                      </div>
                      {submited && !(confirmPassword === password) && (
                        <small className="badge badge-danger">
                          {" "}
                          Las contraseñas deben de ser iguales{" "}
                        </small>
                      )}
                      <div className="input-group mb-4">
                        <div className="input-group-prepend">
                          <span className="input-group-text">@</span>
                        </div>
                        <input
                          className="form-control"
                          placeholder="Ingrese un correo"
                          type="text"
                          name="email"
                          value={email}
                          onChange={this.handleChange}
                        />
                      </div>
                      {submited && !email && (
                        <small className="badge badge-danger">
                          {" "}
                          Debe de ingresar un correo{" "}
                        </small>
                      )}
                      <div className="input-group mb-4">
                        <div className="input-group-prepend">
                          <span className="input-group-text"><i className="icon-doc" /></span>
                        </div>
                        <input
                          className="form-control"
                          placeholder="Ingrese su dni"
                          type="text"
                          name="dni"
                          value={dni}
                          onChange={this.handleChange}
                        />
                      </div>
                      {submited && !dni && (
                        <small className="badge badge-danger">
                          {" "}
                          Debe de ingresar un dni{" "}
                        </small>
                      )}
                      <div className="input-group mb-4">
                        <div className="input-group-prepend">
                          <span className="input-group-text"><i className="icon-link" /></span>
                        </div>
                        <input
                          className="form-control"
                          placeholder="Ingrese la url dina"
                          type="text"
                          name="dina"
                          value={dina}
                          onChange={this.handleChange}
                        />
                      </div>
                      {submited && !dina && (
                        <small className="badge badge-danger">
                          {" "}
                          Debe de ingresar una url{" "}
                        </small>
                      )}
                      <div className="input-group mb-4">
                        <div className="input-group-prepend">
                          <span className="input-group-text"><i className="icon-link" /></span>
                        </div>
                        <input
                          className="form-control"
                          placeholder="Ingrese la url orcid"
                          type="text"
                          name="orcid"
                          value={orcid}
                          onChange={this.handleChange}
                        />
                      </div>
                      {submited && !orcid && (
                        <small className="badge badge-danger">
                          {" "}
                          Debe de ingresar una url{" "}
                        </small>
                      )}
                      <div className="input-group mb-4">
                        <div className="input-group-prepend">
                          <span className="input-group-text"><i className="icon-phone" /></span>
                        </div>
                        <input
                          className="form-control"
                          placeholder="Ingrese su celular"
                          type="text"
                          name="cellphone"
                          value={cellphone}
                          onChange={this.handleChange}
                        />
                      </div>
                      {submited && !cellphone && (
                        <small className="badge badge-danger">
                          {" "}
                          Debe de ingresar un celular{" "}
                        </small>
                      )}
                      <div onChange={this.handleChange}>
                        <div className="form-check form-check-inline">
                          <input  lass="form-check-input" type="radio" name="idRole" id="idEmprendedor" value="2"/>
                          <label className="form-check-label" htmlFor="idRol">Emprendedor Jr</label>
                        </div>
                        <div className="form-check form-check-inline">
                          <input className="form-check-input" type="radio" name="idRole" id="idMentor" value="3"/>
                          <label className="form-check-label" htmlFor="idRole">Mentor</label>
                        </div>
                      </div>
                      <button
                        onClick={this.handleSubmit}
                        className="mt-2 btn btn-block btn-success"
                        type="button"
                      >
                        Crear cuenta
                      </button>
                    </div>
                  </TabList>
                </div>

                <div className="card-footer p-4" />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Register;
