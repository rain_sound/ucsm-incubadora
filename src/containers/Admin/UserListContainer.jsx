import React, { Component } from 'react'
import {withRouter} from 'react-router-dom'
import {connect} from 'react-redux'
import UserList from '../../components/Admin/UserList';
import {getUsersList, userIsAdmin} from '../../reducers'
import {adminActions} from '../../actions/admin'
import {roleActions} from '../../actions/role'


class UserListContainer extends Component {
    constructor(props){
        super(props)
        this.loadUsers()
        
    }
    loadUsers(){
        if(!this.props.admin){
            alert("USUARIO NO ES ADMINISTRADOR")
            window.close()
        }
        else{
            this.props.loadUsersList()
            this.props.loadRoles()
        }
    }
    render() {
        return (
        <div>
            {/* RENDERIZAR LA LISTA */}
            {(Object.entries(this.props.users).length === 0 && this.props.users.constructor === Object) ? "" : <UserList users={this.props.users }></UserList>}
        </div>
        )
    }
}

const mapStateToProps = state => ({
    users: getUsersList(state),
    admin: userIsAdmin(state)
})


const mapDispatchToProps = dispatch => ({
    loadUsersList: () => dispatch(adminActions.getAllUsers()),
    loadRoles: () => dispatch(roleActions.getAllRoles())
})


export default connect(mapStateToProps, mapDispatchToProps)(UserListContainer)