import React, { Component } from 'react'
import {withRouter} from 'react-router-dom'
import PostList from '../../../components/Foro_2/Posts/PostList'
import { postActions } from '../../../actions/posts'
import {connect} from 'react-redux'
import {pagesConstants} from '../../../constants/entrada/foroPages'
import {getListPosts, getUserId, getCountPosts, getRole} from '../../../reducers'
import {pagesConstants as PP} from '../../../constants/entrada/pages'
import PostTable from '../../../components/Foro_2/Posts/PostTable';


/**props
 * type
 */

class PostsListContainer extends Component {

    constructor(props){
        super(props)
        let page = parseInt( props.match.params.id )
        this.state = {
            page
        }
        this.loadPosts()
    }

    componentDidUpdate(){
        if (this.props.match.params.id != this.state.page){
            this.loadPosts()
            this.setState ({
                page: this.props.match.params.id
            })
        }
    }

    loadPosts(){
        let page = parseInt( this.props.match.params.id )
        let limit = PP.PAGES_SIZE
        var offset = (page-1)*limit
        switch(this.props.type){
            case pagesConstants.ALL_POSTS:
            {
                this.props.loadCountPostList()
                this.props.loadPostsList(offset, limit)
                break
            }
            default:
            break
        }
    }
    renderList(){
        switch(this.props.type){
            case pagesConstants.ALL_POSTS:
            {
                return (<PostList posts={this.props.posts}></PostList>)
            }
            default:
                return null
        }

    }
    
    renderPages(page, limit){
        var arr = []
        let index = (page === 1)?0:(((page-1)===1)?(page-1):(page-2))
        let lmt = (page === limit)?limit:(page+2)
        for ( index; index < lmt; index++) {  
            arr.push(index+1);          
        }
        return arr.map((n) => {
            if (n === page)
                return <li class="page-item active"><a class="page-link" href={`#/foro/${n}`}  >{n}</a></li>
            else
                return <li class="page-item"><a class="page-link" href={`#/foro/${n}`}   >{n}</a></li>
        })
    }
    renderPagination(){
        let page = parseInt( this.props.match.params.id )
        var limit = parseInt( this.props.count.size )
        limit = Math.ceil(limit/PP.PAGES_SIZE)
        return (
            <nav aria-label="...">
                <ul class="pagination justify-content-center">

                    <li class={`page-item ${page===1?'disabled':''}`}>
                    <a class="page-link" href={`#/foro/${(page-1)}`} tabindex="-1"  onClick={this.rel}>Previous</a>
                    </li>

                    {this.renderPages(page, limit)}
                   
                    <li class={`page-item ${page===limit?'disabled':''}`}>
                    <a class="page-link" href={`#/foro/${(page+1)}`}  onClick={this.rel}>Next</a>
                    </li>

                </ul>
            </nav>
        )
    }
    render() {
        return (
            <div>
                {(typeof this.props.count === 'undefined') ? "":this.renderPagination()}            
                {(Object.entries(this.props.posts).length === 0 && this.props.posts.constructor === Object) ? "" : this.renderList()}
            </div>
        )
    }
}

const mapStateToProps = state => ({
    posts: getListPosts(state),
    userId: getUserId(state),
    count: getCountPosts(state)
})


const mapDispatchToProps = dispatch => ({
    loadPostsList: (offset, limit) => dispatch(postActions.loadPostsList(offset, limit)),
    loadCountPostList: () => dispatch(postActions.getAllPostsSize()),
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(PostsListContainer))