import React, { Component } from 'react'
import {withRouter} from 'react-router-dom'
import { postActions } from '../../../actions/posts'
import {connect} from 'react-redux'
import {pagesConstants} from '../../../constants/entrada/foroPages'
import {getListPosts, getUserId, getCountPosts} from '../../../reducers'
import {pagesConstants as PP} from '../../../constants/entrada/pages'
import PostTable from '../../../components/Foro_2/Posts/PostTable';



class PostTableContainer extends Component {

    constructor(props){
        super(props)
        let page = parseInt( props.match.params.id )
        this.state = {
            page
        }
        this.loadPosts()
        this.handler = this.handler.bind(this)
    }
    handler(){
        // alert("handler")
        // this.loadPosts()
        window.location.reload()
    }  
    componentDidUpdate(){
        // alert("didmount")
        if (this.props.match.params.id != this.state.page){
            this.loadPosts()
            this.setState ({
                page: this.props.match.params.id
            })
        }
    }

    loadPosts(){
        // alert("LoadPosts")
        let page = parseInt( this.props.match.params.id )
        let limit = PP.PAGES_SIZE
        var offset = (page-1)*limit
        switch(this.props.type){
            case pagesConstants.MY_POSTS:
            {
                this.props.loadCountUserPostList(this.props.userId)
                this.props.loadPostsByUser(this.props.userId, offset, limit)
                break
            }
            default:
            break
        }
    }
    renderList(){
        // alert("renderList")
        switch(this.props.type){
            case pagesConstants.MY_POSTS:
            {
                return (<PostTable posts={this.props.posts} handler={this.handler}></PostTable>)
            }
            default:
            return null;
        }

    }
    
    renderPages(page, limit){
        // alert("pages")
        var arr = []
        let index = (page === 1)?0:(page-3)
        let lmt = (page === limit)?limit:(page+2)
        for (index; index < lmt; index++) {  
            arr.push(index+1);          
        }
        return arr.map((n) => {
            if (n === page)
                return <li class="page-item active"><a class="page-link" href={`#/myposts/${n}`}  >{n}</a></li>
            else
                return <li class="page-item"><a class="page-link" href={`#/myposts/${n}`}   >{n}</a></li>
        })
    }
    renderPagination() {
        let page = parseInt( this.props.match.params.id )
        var limit = parseInt( this.props.count.size )
        limit = Math.ceil(limit/PP.PAGES_SIZE)
        return (
            <nav aria-label="...">
                <ul class="pagination justify-content-center">

                    <li class={`page-item ${page===1?'disabled':''}`}>
                    <a class="page-link" href={`#/myposts/${(page-1)}`} tabindex="-1"  onClick={this.rel}>Previous</a>
                    </li>

                    {this.renderPages(page, limit)}
                   
                    <li class={`page-item ${page===limit?'disabled':''}`}>
                    <a class="page-link" href={`#/myposts/${(page+1)}`}  onClick={this.rel}>Next</a>
                    </li>

                </ul>
            </nav>
        )
    }
    render() {
        // alert("render")
        return (
        <div>
            {(typeof this.props.count === 'undefined') ? "":this.renderPagination()}
            {(Object.entries(this.props.posts).length === 0 && this.props.posts.constructor === Object) ? null : this.renderList()}
        </div>
        )
    }
}

const mapStateToProps = state => ({
    posts: getListPosts(state),
    userId: getUserId(state),
    count: getCountPosts(state),
})


const mapDispatchToProps = dispatch => ({
    loadPostsByUser: (userId, offset, limit)=> dispatch(postActions.loadPostsByUser(userId, offset, limit)),
    loadCountUserPostList : (userId) => dispatch(postActions.getUserPostsSize(userId)),
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(PostTableContainer))