import React, { Component } from 'react'
import {withRouter} from 'react-router-dom'
import PostPage from '../../../components/Foro_2/Posts/PostPage'
import { connect } from 'react-redux'
import { postActions } from '../../../actions/posts'
import { getSelectedPost } from '../../../reducers'
import {getUserId,  userIsAdmin} from '../../../reducers'
import { Form, Button, FormGroup, Input, FormText } from 'reactstrap'
import { commentActions } from '../../../actions/comments'
import CommentsListContainer from '../Comments/CommentsListContainer'
import PostForm from '../../../components/Foro_2/Posts/PostForm'

/**PARAMTERS
 * id-- get by props.match.params.id
 */
// let post = {
//   head: 'head', 
//   body: 'body', 
//   user: {name: 'username'}, 
//   createdAt: '2018/08/08'
// }
class PostPageContainer extends Component {
  constructor(props){
    super(props)
    this.state = {
      comment: ""
    }
    this.comentar = this.comentar.bind(this)
    this.deletePost = this.deletePost.bind(this)
    this.renderPostPage = this.renderPostPage.bind(this)
  }
  handleChange = (e) => {
    const { name, value } = e.target
    this.setState({
        [name]: value
    })
  }
  loadData(id){
    this.props.loadPostById(id)
  }
  componentWillMount(){
    this.loadData(this.props.match.params.id)
  }
  comentar(e){
    e.preventDefault();
    let {userId, selectedPost} = this.props
    this.props.addComment(selectedPost.id, this.state.comment, userId)
    window.location.reload()
  }
  postIsLoaded(){
    return this.props.selectedPost.hasOwnProperty('user')
  }
  isOwner(){
    return (this.props.userId === this.props.selectedPost.userId)
  }
  deletePost(){
    this.props.deletePost(this.props.selectedPost.id)
    this.props.history.push('/foro')
  }
  renderButtons(){
    let modaleditparams = {
      modal: false, 
      edit: true,
      head: this.props.selectedPost.head,
      body: this.props.selectedPost.body,
      id: this.props.selectedPost.id,
    }
    return (
      <div className="card-footer"> 
        <PostForm params={modaleditparams}>Editar</PostForm>
        <button className="btn btn-primary" onClick={this.deletePost}><i className="material-icons">delete</i></button>        
      </div>
    )
  }
  renderPostPage(){
    return (
      <div class="card mb-3" style={{maxWidth: "100%"}}>
        <PostPage post={this.props.selectedPost}></PostPage>
        {this.isOwner() ? (this.renderButtons()) : null}
      </div>
    )
  }
  renderSpinner(){
    return (
      <div class="text-center">
        <div class="spinner-border" role="status">
          <span class="sr-only">Loading...</span>
        </div>
      </div>
    )
  }
  renderVisibility(){
    if(this.props.admin)
      return (
        <div>
            <button className="btn btn-primary" onClick={this.comentar}> <i className="material-icons">visibility</i></button>
        </div>
      )
    else
      return (<div></div>)
  }
  render() {
    return (
      <div>
        {this.renderVisibility()}
        <br/>
        {this.postIsLoaded()?this.renderPostPage():this.renderSpinner()}
        <br />
        <div>
          {/* form o text para registrar comentario */}
          <form onSubmit = {this.comentar}>
            <div className="form-group md-5">
              <input type="text" className="form-control" name="comment" id="post_comment" placeholder="Comentario" onChange={this.handleChange}></input>
            </div>
            <button className="btn btn-primary" onClick={this.comentar}> <i className="material-icons">comment</i></button>
          </form>
          <br />
          {/* aqui ingresar la lista de comentarios */}
          <CommentsListContainer postId={this.props.selectedPost.id}></CommentsListContainer>
        </div>
      </div>
    )
  }
}

const mapDispatchToProps = dispatch => ({
  loadPostById: id => (dispatch(postActions.loadPostById(id))),
  addComment: (respId, comment, UserId) => (dispatch(commentActions.addComment(respId, comment, UserId))),
  deletePost: (id) => (dispatch(postActions.deletePost(id))),
})
const mapStateToProps = state => ({
  userId: getUserId(state),
  selectedPost: getSelectedPost(state),
  admin: userIsAdmin(state),
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(PostPageContainer))