import React, { Component } from 'react'
import { commentActions } from '../../../actions/comments'
import CommentList from '../../../components/Foro_2/Comments/CommentList'
import {getCommentsList} from '../../../reducers'
import {connect} from 'react-redux'
class CommentsListContainer extends Component {
    componentWillMount(){
        this.loadComments(this.props.postId)
    }
    loadComments(postId){
        this.props.loadListOfComments(postId)
    }
    renderComments(){
        return (<CommentList comments={this.props.comments}></CommentList>)
    }
    render() {
        return (
        <div>
            { (Object.entries(this.props.comments).length === 0 && this.props.comments.constructor === Object) ? null : this.renderComments() }
        </div>
        )
    }
}

const mapStateToProps = state => ({
    comments: getCommentsList(state),
})


const mapDispatchToProps = dispatch => ({
    loadListOfComments: (postId) => dispatch(commentActions.loadListOfComments(postId))
})

export default connect(mapStateToProps, mapDispatchToProps)(CommentsListContainer)