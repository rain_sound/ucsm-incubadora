const ALL_POSTS = "ALL_POSTS"
const MY_POSTS = "MY_POSTS"

export const pagesConstants = {
    ALL_POSTS,
    MY_POSTS
}