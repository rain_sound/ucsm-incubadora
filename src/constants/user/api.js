import { URL } from '../../helpers/api'

const SIGNIN                    = `${URL}users/signin` 
const SIGNUP                    = `${URL}users/signup` 
const FORGOTPASSWORD            = `${URL}users/forgotpassword` 
const RESETPASSWORD             = `${URL}users/resetpassword` 
const UPDATEPASSWORD            = `${URL}users/updatepassword` 
const UPDATEIMAGE               = `${URL}users/uploadImage` 
const GETUSERSTOAPP             = `${URL}users/getuserspermissions` 
const APPROVEUSER               = `${URL}users/requestpermissions` 
const GETUSERSTOAPPFILTER       = `${URL}users/getusersfilter` 
const GETUSERSCOUT              = `${URL}users/getcountusers` 

export const userConstants = {
    SIGNIN,
    SIGNUP,
    FORGOTPASSWORD,
    RESETPASSWORD,
    UPDATEPASSWORD,
    UPDATEIMAGE,
    GETUSERSTOAPPFILTER,
    GETUSERSTOAPP,
    APPROVEUSER,
    GETUSERSCOUT
}