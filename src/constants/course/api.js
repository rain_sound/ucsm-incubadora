import { URL } from '../../helpers/api'

const CREATE_COURSE                        = `${URL}courses/new` 

const UPDATE_COURSE                 = id => `${URL}courses/${id}` 
const GET_ALL_STUDENTS_BY_COURSE    = id => `${URL}courses/${id}/students` 
const GET_ALL_COURSES_BY_TEACHER    = id => `${URL}courses/${id}/teacher` 

export const courseConstants = {
    CREATE_COURSE,
    UPDATE_COURSE,
    GET_ALL_STUDENTS_BY_COURSE,
    GET_ALL_COURSES_BY_TEACHER
}

