const FORO_MP = 'FORO_MAIN_PAGE'
const USER_INFO = 'USER_INFO'

const ALL_ENTRIES = 'ALL_ENTRIES'
const MY_ENTRIES = 'MY_ENTRIES'
const AN_ENTRY = 'AN_ENTRY'

export const entradaConstants = {
    FORO_MP,
    USER_INFO,
}

export const mainPageConstants = {
    ALL_ENTRIES,
    MY_ENTRIES,
    AN_ENTRY,
}