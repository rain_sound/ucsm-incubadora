import { courseConstants } from "../constants/course/actions";
import update from 'immutability-helper'


const initialize = {
    list: []
  }
  

export const course = (state = initialize, action) => {
  switch (action.type) {
    case courseConstants.GET_ALL_COURSES_REQUEST: {
        return { loading: true };
      }
      case courseConstants.GET_ALL_COURSES_FAILED: {
        return { loading: false };
      }
      case courseConstants.GET_ALL_COURSES_SUCCESS: {
        const { data } = action.payload
        return { list: data };
      }
      case courseConstants.UPDATE_COURSE_REQUEST: {
        return { ...state, updating: true };
      }
      case courseConstants.UPDATE_COURSE_FAILED: {
        const { data } = action.payload
        return { ...state, updating: false, error: data };
      }
      case courseConstants.UPDATE_COURSE_SUCCESS: {
        const { data } = action.payload
        const index =   [...state.list].findIndex(item => item.id === data.id)
        return { list: update(state.list, {
          [index]: {
            name:        { $set: data.name },
            description: { $set: data.description },
            status:      { $set: data.status }
          }
          })
        }
      }
      default:
        return state;
    }
};

export const getUserId = state => state.id
export const getCourseById = (state, id) => state.list.filter(item => item.id === id)