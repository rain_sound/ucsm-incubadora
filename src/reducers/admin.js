import { adminActionConstants as AAC } from '../constants/admin/actionConstants'

export const usersList = (state = {}, action) => {
    switch (action.type) {
        case AAC.GET_ALLUSERS_SUCCESS:
            return action.users
        default:
            return state
    }
}