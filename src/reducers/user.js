import { userConstants } from "../constants/user/actions";


export const user = (state = {}, action) => {
    switch (action.type) {
        case userConstants.LOGGING:
            {
                const { name } = action.payload;
                return {...state, name };
            }
            case userConstants.FAILURE: {
              const { reason } = action.payload;
              return { reason };
            }
            case userConstants.LOGED: {
              const { auth, id, email, imgProfile, rol, accept } = action.payload;
              return { ...state, auth, id, email, rol, accept, profile: { filename: imgProfile } };
            }
            case userConstants.LOGOUT: {
              return { };
            }
            case userConstants.UPDATE_IMAGE_REQUEST: {
              return { ...state, profile: { ...state.profile, loading: false } };
            }
            case userConstants.UPDATE_IMAGE_SUCCESS: {
              const { filename, message } = action.payload;
              return { ...state, profile: { completed: true, filename, message, reason: null } };
            }
            case userConstants.UPDATE_IMAGE_FAILURE: {
              const { reason } = action.payload;
              return { ...state, profile: { ...state.profile, loading: false, message: null, reason } };
            }
            case userConstants.CLEAR_IMAGE: {
              return { ...state, profile: { ...state.profile, message: null, reason: null } }
            }
            
            case userConstants.GET_ALL_USERS_TO_APPROV_SUCCESS: {
              return { ...state, usersToApprov: action.payload }
            }
        
            case userConstants.GET_ALL_USERS_COUNT: {
              return { ...state, loading: true  }
            }
            case userConstants.GET_ALL_USERS_COUNT_SUCCESS: {
              return { ...state, count: action.payload, loading: false }
            }
            case userConstants.GET_ALL_USERS_COUNT_FAILURE: {
              return { ...state, loading: false }
            }
            case userConstants.GET_ALL_USERS_TO_APPROV_FILTER: {
              return { ...state, loading: true  }
            }
            case userConstants.GET_ALL_USERS_TO_APPROV_FILTER_SUCCESS: {
              return { ...state, usersToApprov: action.payload, loading: false }
            }
            case userConstants.GET_ALL_USERS_TO_APPROV_FILTER_FAILURE: {
              return { ...state, loading: false }
            }
            
            default:
              return state;
          }
        };

export const getUserId = state => state.id
export const getUsername = state => state.name
export const getRole = ({ accept, role }) => ({ accept, role })
export const userIsAdmin = state => (state.roleId === 1)
