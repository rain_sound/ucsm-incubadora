import { combineReducers } from 'redux'
import { user, getRole as _getRole, getUserId as _getUserId, getUsername as _getUsename, userIsAdmin as _userIsAdmin } from './user'
import { alert } from './alert'
import { posts, postSeleted, commentsList, postCount, getImgPost as _getImgPost, getCountPosts as _getCountPosts, getListPosts as _getListPosts } from './entrada'
import { userConstants } from '../constants/user/actions'
import { usersList } from './admin'
import { rolesList } from './role'

const appReducer = combineReducers({
    user,
    posts,
    postSeleted,
    commentsList,
    postCount,
    alert,
    usersList,
    rolesList
})

export const rootReducer = (state, action) => {
        if (action.type === userConstants.LOGOUT) {
            state = undefined
        }

        return appReducer(state, action)
    }
    //Selectors
export const userIsAdmin = state => _userIsAdmin(state.user)
export const getUserId = state => _getUserId(state.user)
export const getUsername = state => _getUsename(state.user)
export const getListPosts = state => _getListPosts(state.posts)
export const getSelectedPost = state => (state.postSeleted)
export const getCommentsList = state => (state.commentsList)
export const getImgPost = state => _getImgPost(state.postSeleted)
export const getCountPosts = state => _getCountPosts(state.postCount)
export const getRole = state => _getRole(state.user)
export const getUsersList = state => (state.usersList)
export const getRolesList = state => (state.rolesList)
