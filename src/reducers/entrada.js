import { entryActionConstants as EAC } from '../constants/entrada/actionConstants'
/*export const loadList = (state = {}, action) => {
    switch (action.type) {
        case entryActionConstants.LIST_LOADED:
            {
                return ({...state, entries: action.entries })
            }
        default:
            {
                return state
            }
    }
}

export const mPage = (state = {}, action) => {
    switch (action.type) {
        case EAC.FORO_MAIN_PAGE:
            {
                return {...state, foro_mp: action.payload, data: action.data }
            }
        default:
            {
                return state
            }

    }
}*/
export const posts = (state = {}, action) => {
    switch (action.type) {
        case EAC.POSTS_LIST_LOADED:
            {
                return action.posts
            }
        default:
            {
                return state
            }
    }
}

export const postCount = (state = {}, action) => {
    switch (action.type) {
        case EAC.GET_ALLCOUNT_SUCCESS:
            {
                return { count: action.payload }
            }
        case EAC.GET_USERCOUNT_SUCCESS:
            {
                return { count: action.payload }
            }
        default:
            {
                return state
            }
    }
}

export const postSeleted = (state = {}, action) => {
    switch (action.type) {
        case EAC.POST_SELECTED:
            {
                return action.selected
            }
        case EAC.POST_OBTAINED:
            {
                return action.post
            }
        case EAC.UPDATE_IMAGE_SUCCESS:
            {
                return {...state, postimg: action.payload.filename }
            }
        default:
            {
                return state
            }
    }
}
export const commentsList = (state = {}, action) => {
    switch (action.type) {
        case EAC.COMMENTS_LIST_LOADED:
            {
                return action.comments
            }
        default:
            {
                return state
            }
    }
}

export const getListPosts = state => (state)
export const getImgPost = state => (state.postimg)
export const getCountPosts = state => (state.count)