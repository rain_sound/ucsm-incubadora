import { roleActionConstants as RAC } from '../constants/role/actionConstants'

export const rolesList = (state = {}, action) => {
    switch (action.type) {
        case RAC.GET_ROLELIST_SUCCESS:
            return action.roles
        default:
            return state
    }
}