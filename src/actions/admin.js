import { adminActionConstants as AAC } from '../constants/admin/actionConstants'
const AdminService = require('../services/adminServices')

const getAllUsers = () => {
    const request = () => ({ type: AAC.GET_ALLUSERS_REQUEST })
    const successful = (users) => ({ type: AAC.GET_ALLUSERS_SUCCESS, users })
    const failed = (error) => ({ type: AAC.GET_ALLUSERS_FAILURE, error })
    return dispatch => {
        dispatch(request())
        AdminService.getAllUsers()
            .then(users => {
                dispatch(successful(users))
            })
            .catch(err => {
                dispatch(failed(err))
            })
    }
}
const banUser = (userId, banned) => {
    const request = () => ({ type: AAC.BAN_USER_REQUEST })
    const seccessful = () => ({ type: AAC.BAN_USER_SUCCESS })
    const failed = (error) => ({ type: AAC.BAN_USER_FAILURE, error })
    return dispatch => {
        dispatch(request())
        AdminService.banUser(userId, banned)
            .then(() => dispatch(seccessful()))
            .catch((err) => dispatch(failed(err)))
    }
}
const setRoleUser = (userId, roleId) => {
    const request = () => ({ type: AAC.SET_USERROLE_REQUEST })
    const successful = () => ({ type: AAC.SET_USERROLE_SUCCESS })
    const failed = (err) => ({ type: AAC.SET_USERROLE_FAILURE, err })
    return dispatch => {
        dispatch(request())
        AdminService.setRoleUser(userId, roleId)
            .then(() => dispatch(successful()))
            .catch((err) => dispatch(failed(err)))
    }
}

const setMarkPost = (postId, mark) => {
    const request = () => ({ type: AAC.SET_POSTMARK_REQUEST})
    const successful = () => ({ type: AAC.SET_POSTMARK_SUCCESS})
    const failed = (err) => ({ type: AAC.SET_POSTMARK_FAILURE})
    return dispatch => {
        dispatch(request())
        AdminService.setMarkPost(postId, mark)
        .then(() => dispatch(successful()))
        .catch((err)=> dispatch(failed(err)))
    }
}

export const adminActions = {
    getAllUsers,
    banUser,
    setRoleUser,
    setMarkPost,
}