import { courseConstants } from '../constants/course/actions'
import { courseServices } from '../services/courseServices'


const getAll = id => {

    const request = ()      => ({ type: courseConstants.GET_ALL_COURSES_REQUEST })
    const success = payload => ({ type: courseConstants.GET_ALL_COURSES_SUCCESS, payload })
    const failure = ()      => ({ type: courseConstants.GET_ALL_COURSES_FAILED })

    return dispatch => {
        dispatch(request())
        courseServices.getAllCoursesByTeacher(id)
            .then(data => {
                const { err } = data
                if(!err) dispatch(success(data))
                else dispatch(failure())
            })
    }
}

const updateData = (id, name, description, status) => {
    
    const request = ()      => ({ type: courseConstants.UPDATE_COURSE_REQUEST })
    const success = payload => ({ type: courseConstants.UPDATE_COURSE_SUCCESS, payload })
    const failure = payload => ({ type: courseConstants.UPDATE_COURSE_FAILED, payload })

    return dispatch => {
        dispatch(request())
        courseServices.updateCourse(id, name, description, status)
            .then(data => {
                const { err } = data
                if(!err) dispatch(success(data))
                else dispatch(failure(err))
            })
    }

}

export const courseActions = {
    getAll,
    updateData
}
