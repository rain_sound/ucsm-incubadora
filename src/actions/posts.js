import { entryActionConstants as EAC } from '../constants/entrada/actionConstants'
const PostService = require('../services/postServices')

const loadPostsList = (offset, limit) => {
    const request = () => ({ type: EAC.LOADING_POSTS_LIST })
    const successful = (posts) => ({ type: EAC.POSTS_LIST_LOADED, posts })
    const failed = (error) => ({ type: EAC.POSTS_LIST_LOAD_FAILED, error })
    return dispatch => {
        dispatch(request())
        PostService.getAllPosts(offset, limit)
            .then(posts => {
                dispatch(successful(posts))
            })
            .catch(err => {
                dispatch(failed(err))
            })
    }
}

const loadPostsByUser = (userId, offset, limit) => {
    const request = () => ({ type: EAC.LOADING_POSTS_LIST })
    const successful = (posts) => ({ type: EAC.POSTS_LIST_LOADED, posts })
    const failed = (error) => ({ type: EAC.POSTS_LIST_LOAD_FAILED, error })
    return dispatch => {
        dispatch(request())
        PostService.getUserPosts(userId, offset, limit)
            .then(posts => {
                dispatch(successful(posts))
            })
            .catch(err => {
                dispatch(failed(err))
            })
    }
}

const registerPost = (userId, head, body) => {
    const request = () => ({ type: EAC.REGISTERING_POST, userId })
    const successful = (resp) => ({ type: EAC.POST_REGISTERED, resp })
    const failed = (error) => ({ type: EAC.POST_REGISTER_FAILED, error })
    return dispatch => {
        dispatch(request())
        PostService.regPost(userId, head, body)
            .then(resp => {
                dispatch(successful(resp))
            })
            .catch(err => {
                dispatch(failed(err))
            })
    }
}

const selectedPost = post => ({ type: EAC.POST_SELECTED, selected: post })

const loadPostById = id => {
    const request = () => ({ type: EAC.POST_REQUIRED })
    const successful = (post) => ({ type: EAC.POST_OBTAINED, post })
    const failed = (err) => ({ type: EAC.POST_REQUEST_FAILED })
    return dispatch => {
        dispatch(request())
        PostService.getPostById(id)
            .then(resp => dispatch(successful(resp)))
            .catch(err => dispatch(failed(err)))
    }
}

const updatePost = (id, head, body) => {
    const request = () => ({ type: EAC.UPDATING_POST, id })
    const successful = (resp) => ({ type: EAC.POST_UPDATED, resp })
    const failed = (err) => ({ type: EAC.POST_UPDATE_FAILED, err })
    return dispatch => {
        dispatch(request())
        PostService.updatePost(id, head, body)
            .then(resp => {
                dispatch(successful(resp))
            })
            .catch(err => {
                dispatch(failed(err))
            })
    }
}

const deletePost = (id) => {
    const request = () => ({ type: EAC.DELETING_POST, id })
    const successful = (resp) => ({ type: EAC.POST_DELETED, resp })
    const failed = (err) => ({ type: EAC.POST_DELETE_FAILED, err })
    return dispatch => {
        dispatch(request())
        PostService.deletePost(id)
            .then(resp => {
                dispatch(successful(resp))
            })
            .catch(err => {
                dispatch(failed(err))
            })
    }
}

const updateImage = (image, id) => {

    const request = () => ({ type: EAC.UPDATE_IMAGE_REQUEST })
    const success = payload => ({ type: EAC.UPDATE_IMAGE_SUCCESS, payload })
    const failure = payload => ({ type: EAC.UPDATE_IMAGE_FAILURE, payload })

    return dispatch => {
        dispatch(request())
        PostService.updateImage(image, id)
            .then(data => {
                const { filename, reason, message } = data
                if (filename) {
                    dispatch(success({ filename, message }))
                } else dispatch(failure({ reason }))
            })
    }
}

const getAllPostsSize = () => {
    const request = () => ({ type: EAC.GET_ALLCOUNT_REQUEST })
    const success = payload => ({ type: EAC.GET_ALLCOUNT_SUCCESS, payload })
    const failure = payload => ({ type: EAC.GET_ALLCOUNT_FAILURE, payload })

    return dispatch => {
        dispatch(request())
        PostService.getAllPostsSize()
            .then(data => dispatch(success(data)))
            .catch(err => dispatch(failure(err)))
    }
}

const getUserPostsSize = (userId) => {
    const request = () => ({ type: EAC.GET_USERCOUNT_REQUEST })
    const success = payload => ({ type: EAC.GET_USERCOUNT_SUCCESS, payload })
    const failure = payload => ({ type: EAC.GET_USERCOUNT_FAILURE, payload })

    return dispatch => {
        dispatch(request())
        PostService.getUserPostsSize(userId)
            .then(data => dispatch(success(data)))
            .catch(err => dispatch(failure(err)))
    }
}

export const postActions = {
    loadPostsList,
    loadPostsByUser,
    registerPost,
    selectedPost,
    loadPostById,
    updatePost,
    deletePost,
    updateImage,
    getAllPostsSize,
    getUserPostsSize
}