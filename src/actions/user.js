import { userService } from '../services/userService'
import { userConstants } from '../constants/user/actions'

const login = (name, password, history) => {

    const request = payload => ({ type: userConstants.LOGGING, payload })
    const success = payload => ({ type: userConstants.LOGED, payload })
    const failure = payload => ({ type: userConstants.FAILURE, payload })

    return dispatch => {
        dispatch(request({ name }))
        userService.login(name, password)
            .then(data => {
                const { auth, reason, name, id, email, imgProfile, rol, accept } = data
                if(auth) { 
                    console.log('data', data);
                    dispatch(success({ auth, name, id, email, imgProfile, rol, accept }))
                    history.push('dashboard')
                }
                else dispatch(failure({ reason })) 


            })
    }
}

const logout = () => {
    localStorage.clear()
    const request = () => ({ type: userConstants.LOGOUT })
    return dispatch => {
        dispatch(request())
    }
}


const updateImage = (image, id) => {

    const request = () => ({ type: userConstants.UPDATE_IMAGE_REQUEST })
    const success = payload => ({ type: userConstants.UPDATE_IMAGE_SUCCESS, payload })
    const failure = payload => ({ type: userConstants.UPDATE_IMAGE_FAILURE, payload })

    return dispatch => {
        dispatch(request())
        userService.updateImage(image, id)
            .then(data => {
                const { filename, reason, message } = data
                if (filename) {
                    dispatch(success({ filename, message }))
                } else dispatch(failure({ reason }))
            })
    }
}


const clearImage = () => {
    const request = () => ({ type: userConstants.CLEAR_IMAGE })

    return dispatch => {
        dispatch(request())
    }
}

const getAllUsersAppr = (offset = 0, limit = 8) => {
    const request = ()      => ({ type: userConstants.GET_ALL_USERS_TO_APPROV })
    const success = payload => ({ type: userConstants.GET_ALL_USERS_TO_APPROV_SUCCESS, payload })
    const failure = payload => ({ type: userConstants.GET_ALL_USERS_TO_APPROV_FAILURE, payload })

    return dispatch => {
        dispatch(request())
        userService.getUsersToApproved(offset, limit)
            .then(data => {
                if(data) {
                    dispatch(success(data.data))
                }
                else dispatch(failure(data))
            })
    }
}

const getAllUserApprFilter = text  => {
    const request = ()      => ({ type: userConstants.GET_ALL_USERS_TO_APPROV_FILTER })
    const success = payload => ({ type: userConstants.GET_ALL_USERS_TO_APPROV_FILTER_SUCCESS, payload })
    const failure = payload => ({ type: userConstants.GET_ALL_USERS_TO_APPROV_FILTER_FAILURE, payload })

    return dispatch => {
        dispatch(request())
        userService.getUsersToApprovedFilter(text)
            .then(data => {
                if(data) {
                    dispatch(success(data.data))
                }
                else dispatch(failure(data))
            })
    }
}



const getCount = () => {
    const request = () => ({ type: userConstants.GET_ALL_USERS_COUNT })
    const success = payload => ({ type: userConstants.GET_ALL_USERS_COUNT_SUCCESS, payload })
    const failure = payload => ({ type: userConstants.GET_ALL_USERS_COUNT_FAILURE, payload })

    return dispatch => {
        dispatch(request())
        userService.getCount()
            .then(res => {
                const { data: { data }, message } = res
                if(data) { 
                    dispatch(success(data))
                }
                else dispatch(failure( message )) 

            })
    }
}


export const userActions = {
    login,
    logout,
    updateImage,
    clearImage,
    getAllUsersAppr,
    getCount,
    getAllUserApprFilter
}
