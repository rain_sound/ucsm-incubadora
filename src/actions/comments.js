import { entryActionConstants as EAC } from '../constants/entrada/actionConstants'
const CommentService = require('../services/commentServices')

const loadListOfComments = (resp_id) => {
    const request = () => ({ type: EAC.LOADING_COMMENTS_LIST, resp_id })
    const successful = (comments) => ({ type: EAC.COMMENTS_LIST_LOADED, comments })
    const failed = (err) => ({ type: EAC.COMMENTS_LIST_LOAD_FAILED, err })
    return dispatch => {
        dispatch(request)
        CommentService.getPostComments(resp_id)
            .then(comments => {
                dispatch(successful(comments))
            })
            .catch(err => {
                dispatch(failed(err))
            })
    }
}

const addComment = (resp_id, body, userId) => {
    const request = () => ({ type: EAC.ADDING_COMMENT, resp_id })
    const successful = (resp) => ({ type: EAC.COMMENT_ADDED, resp })
    const failed = (err) => ({ type: EAC.COMMENTS_LIST_LOAD_FAILED, err })
    return dispatch => {
        dispatch(request())
        CommentService.addComment(resp_id, body, userId)
            .then(resp => {
                dispatch(successful(resp))
            })
            .catch(err => {
                dispatch(failed(err))
            })
    }
}

const updateComment = (id, body) => {
    const request = () => ({ type: EAC.UPDATING_POST, id })
    const successful = (resp) => ({ type: EAC.POST_UPDATED, resp })
    const failed = (err) => ({ type: EAC.POST_UPDATE_FAILED, err })
    return dispatch => {
        dispatch(request())
        CommentService.updateComment(id, body)
            .then(resp => {
                dispatch(successful(resp))
            })
            .catch(err => {
                dispatch(failed(err))
            })
    }
}

const deleteComment = (id) => {
    const request = () => ({ type: EAC.DELETING_COMMENT, id })
    const successful = (resp) => ({ type: EAC.COMMENT_DELETED, resp })
    const failed = (err) => ({ type: EAC.COMMENT_DELETE_FAILED, err })
    return dispatch => {
        dispatch(request())
        CommentService.deleteComment(id)
            .then(resp => {
                dispatch(successful(resp))
            })
            .catch(err => {
                dispatch(failed(err))
            })
    }
}

export const commentActions = {
    loadListOfComments,
    addComment,
    updateComment,
    deleteComment,
}