import { roleActionConstants as RAC } from '../constants/role/actionConstants'
const RoleService = require('../services/roleServices')

const getAllRoles = () => {
    const request = () => ({ type: RAC.GET_ROLELIST_REQUEST })
    const successful = (roles) => ({ type: RAC.GET_ROLELIST_SUCCESS, roles })
    const failed = (err) => ({ type: RAC.GET_ROLELIST_FAILURE, err })
    return dispatch => {
        dispatch(request())
        RoleService.getAllRoles()
            .then(roles => dispatch(successful(roles)))
            .catch(err => dispatch(failed(err)))
    }
}

export const roleActions = {
    getAllRoles,
}