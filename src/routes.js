import React from 'react';



const Foro = React.lazy(() =>   import ('./components/Foro_2/Foro'));
const Profile = React.lazy(() =>   import ('./components/Profile/Profile'));
const PasswordRecovery = React.lazy(() =>   import ('./components/Profile/PasswordRecovery'));
const PostForm2 = React.lazy(() =>  import("./components/Foro_2/Posts/PostForm2"));
const ApprovedUsers  = React.lazy(() =>  import("./components/Admin/ApprovedUsers"));

const routes = [
    { path: '/', exact: true, name: 'Home' },
    { path: '/foro/:id', name: 'Foro', component: Foro },
    { path: '/myposts/:id', name: 'Mis Posts', component: Foro },
    { path: '/regpost', name: 'Registrar Post', component: PostForm2 },
    { path: '/profile', exact: true, name: 'Profile', component: PasswordRecovery },
    { path: '/profile/:name/reset', name: 'Reset Password', component: PasswordRecovery },
    { path: '/profile/:name', name: 'Profile Image', component: Profile },
    { path: '/admin/approved/:id', name: 'Aprobar usuarios', component: ApprovedUsers }
];

export default routes;