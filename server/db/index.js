const Sequelize = require('sequelize')
const UserModel = require('./models/user.model')
const RoleModel = require('./models/role.model')
    // const EntradaModel = require('./models/entrada.model')
const PostModel = require('./models/post.model')
const CommentModel = require('./models/comment.model')
const cf = require('../config')



const seedDatabase = require('./factories/index.js');



//Configuracion inicial
const conn = new Sequelize(cf.db_name, cf.user_db, cf.password_db, {
    host: cf.host,
    dialect: cf.dialect,
    pool: {
        max: cf.max_conn,
        min: cf.min_conn
    }
})

//Iniciar modelos | ORM 
const User = UserModel(conn, Sequelize)
const Role = RoleModel(conn, Sequelize)
const Post = PostModel(conn, Sequelize)
const Comment = CommentModel(conn, Sequelize)
    // const Entrada = EntradaModel(conn, Sequelize, User)

/*----------------RELACIONES---------------------*/
//Relacion Usuario - Rol
Role.hasMany(User)
User.belongsTo(Role)
    // User.hasMany(Entrada)
    //Relacion Usuario - Post
User.hasMany(Post)
Post.belongsTo(User)
    //Realcion Usuario - Comment
User.hasMany(Comment)
Comment.belongsTo(User)
    //Relacion Post - Comment
Post.hasMany(Comment)
Comment.belongsTo(Post)



// Entrada.belongsTo(User)

//Force === true Para crear todas las tablas cuidado dropea toda la data
//Force === false Para crear todas las tablas que no existe

conn.sync({ force: false })
    .then(() => {
        seedDatabase(User, Post, Comment);
        console.log('Tablas y database creados')
    })
    .catch(err => {
        console.log(err);
    })

module.exports = {
    User,
    Role,
    Post,
    Comment
}
