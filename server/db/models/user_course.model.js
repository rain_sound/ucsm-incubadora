module.exports = (sequelize, type) => {
    return sequelize.define('user_course', {
        id: {
            type: type.BIGINT,
            primaryKey: true,
            autoIncrement: true
        }
    });
}