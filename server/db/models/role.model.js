module.exports = (sequelize, type) => {
    return sequelize.define('role', {
        id: {
         type: type.BIGINT,
         primaryKey: true,
         autoIncrement: true   
        },
        description: type.STRING(255)
    })

}