module.exports = (sequelize, type) => {
    return sequelize.define('post', {
        id: {
            type: type.BIGINT,
            primaryKey: true,
            autoIncrement: true
        },
        deleted: type.BOOLEAN, //is deleted the post
        mark: type.BOOLEAN, //marca de contenido no apropiado
        head: type.TEXT,
        body: type.TEXT,
        postimg: type.STRING(255)
    });
}