module.exports = (sequelize, type) => {
    return sequelize.define('comment', {
        id: {
            type: type.BIGINT,
            primaryKey: true,
            autoIncrement: true
        },
        deleted: type.BOOLEAN, //is deleted the comment
        body: type.TEXT
    });
}