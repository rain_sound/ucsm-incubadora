module.exports = (sequelize, type) => {
    return sequelize.define('courses', {
        id: {
            type: type.BIGINT,
            primaryKey: true,
            autoIncrement: true
        },
        id_teacher: {
            type: type.BIGINT
        },
        name: {
            type: type.STRING(80),
            unique: true
        },
        description: {
            type: type.STRING(255)
        },
        status: {
            type: type.BOOLEAN
        },
        start_date: {
            type: type.DATE
        },
        end_date:{
            type: type.DATE
        }
    });
}