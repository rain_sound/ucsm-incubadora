module.exports = (sequelize, type) => {
    return sequelize.define('permission', {
        id: {
            type: type.BIGINT,
            primaryKey: true,
            autoIncrement: true
        },
        description: {
            type: type.STRING(255)
        }
    });
}