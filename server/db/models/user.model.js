module.exports = (sequelize, type) => {
    return sequelize.define('users', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: type.STRING(50),
            unique: {
                args: true,
                msg: 'Ya existe una categoría con ese nombre.',
                fields: ['name']
            },
        },
        email: {
            type: type.STRING(150),
            unique: {
                msg: 'This email is already taken.',
                fields: ['email']
            }
        },
        dni: type.STRING(8),
        cellphone: type.STRING(9),
        accept: type.BOOLEAN,
        dina: type.STRING(255),
        orcid: type.STRING(255),
        password: type.STRING(255),
        passwordResetToken: type.STRING(60),
        passwordResetTokenExpire: type.DATE,
        imgProfile: type.STRING(255),
        banned: type.BOOLEAN,
    })
}