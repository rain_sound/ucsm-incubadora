
const { Course, User, UserCourse, Permission, Role } = require("../../db");
const { sendJson } = require("../../utilities")


const getAll = (req, res, next) => {
    Course.findAll().then(data => sendJson(res,{}, false, false, data));
}

const getAllUsersByCourseId = (req, res, next) => {
    if(req.params.id){
        User.findAll({
            attributes: ['name','email','imgProfile'],
            include: [{
                model: Course,
                attributes: [],
                required: true,
                where: {
                    id: req.params.id
                }
            }]
        }).then(users => {
            return sendJson(res, {}, false, false, users);
        })
        .catch(err => {
            return sendJson(res, err);
        })
    }
}

const getAllCoursesByTeacherId = (req, res, next) => {
    if(req.params.id){
        Course.findAll({
             where: {
                    id_teacher: req.params.id,
                },
            include: [{
                attributes: ['id','description'],
                model: Role,
                required: true,
               
            }]
        }).then(users => {
            return sendJson(res, {}, false, false, users);
        })
        .catch(err => {
            return sendJson(res, err);
        })
    }
}

const createCourse = (req, res, next) => {
    Course.create({
        id_teacher: req.body.id,
        name: req.body.name,
        description: req.body.description,
        status: true,
        start_date: req.body.start_date,
        end_date: req.body.end_date
    }).then(course => {
        if(course.id) return sendJson(res, "Se registro con éxito", false)
        else return sendJson(res, "No tiene permitido crear cursos")
    }).catch(err => {
        return sendJson(res, err.original.detail)
    })
}

const updateCourse= (req, res, next) => {
    if(req.params.id){
        Course.findOne({
            where:{
                id: req.params.id
            }
        }).then(course => {
            course.update({
                name: req.body.name,
                description: req.body.description,
                status: req.body.status
            }).then(data => {
                return sendJson(res, "Se actualizó con éxito", false, true, data)
            }).catch(err => {
                console.log('error', err);
                return sendJson(res, "Error en la actualización")
            })
        }).catch(err => sendJson(res, "Curso no encontrado"))
       
    }
}

module.exports = {
    getAll,
    getAllUsersByCourseId,
    getAllCoursesByTeacherId,
    createCourse,
    updateCourse
}