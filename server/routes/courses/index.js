const router = require("express").Router();

const { verifyToken } = require("../middleware");
const courseService = require("./course.service");

router.get("/", courseService.getAll);
router.get("/:id/students", courseService.getAllUsersByCourseId);
router.get("/:id/teacher", courseService.getAllCoursesByTeacherId);

router.post("/new", courseService.createCourse);

router.put("/:id", courseService.updateCourse);

module.exports = router;