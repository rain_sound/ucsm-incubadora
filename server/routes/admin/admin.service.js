const { User } = require('../../db')
const { Post } = require ('../../db')


//getAllUsers POST
getAllUsers = (req, res, next) => {
    User.findAll({}).then(respuestas => res.json(respuestas))
}

//banUser POST
banUser = (req, res, next) => {
    let { id, banned } = req.body
    User.update({ banned }, { where: { id: id } })
        .then((users) => res.send("1"))
        .catch((err) => res.send("0"))
}

//setUserRole POST
setUserRole = (req, res, next) => {
    let { id, roleId } = req.body
    User.update({ roleId }, { where: { id, id } })
        .then(() => res.send("1"))
        .catch(() => res.send("0"))
}

//setMarkPost POST
setMarkPost = (req, res, next) => {
    let { id, mark} = req.body
    Post.update({mark}, {where: {id}})
    .then(() => res.send("1"))
    .catch(() => res.send("0"))
}

module.exports = {
    getAllUsers,
    banUser,
    setUserRole,
    setMarkPost,
}