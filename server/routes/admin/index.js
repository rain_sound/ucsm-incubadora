const router = require('express').Router()
const adminService = require('./admin.service')

router.post('/getallusers', adminService.getAllUsers)
router.post('/banuser', adminService.banUser)
router.post('/setroleuser', adminService.setUserRole)
router.post('admin/setmarkpost', adminService.setMarkPost)

module.exports = router