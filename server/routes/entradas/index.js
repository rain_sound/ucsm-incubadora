const router = require('express').Router()
    // const entradaService = require('./entrada.service')
const postsService = require('./posts.service')
const commentsService = require('./comments.service')

//Estas sub rutas tienen como base localhost/nombre_modelo/
// router.post('/crearentrada', entradaService.crearEntrada)
// router.post('/ingresarrespuesta', entradaService.ingresarRespuesta)
// router.get('/listarentradas', entradaService.listarEntradas)
// router.get('/listarentradasusuario', entradaService.listarEntradasUsuario)
// router.get('/listarrespuestas', entradaService.listarRespuestas)
// router.post('/actualizarentrada', entradaService.actualizarEntrada)
// router.post('/borrarentrada', entradaService.borrarEntrada)

router.get('/post/getallposts', postsService.getAllPosts)
router.get('/post/allpostcount', postsService.getAllPostsSize)
router.get('/post/getuserposts', postsService.getUserPosts)
router.get('/post/userpostcount', postsService.getUserPostsSize)
router.post('/post/regpost', postsService.regPost)
router.get('/post/getpostbyid', postsService.getPostById)
router.post('/post/updatepost', postsService.updatePost)
router.post('/post/deletepost', postsService.deletePost)
router.post("/post/uploadimage", postsService.uploadImage);

router.get('/comment/getpostcomments', commentsService.getPostComments)
router.post('/comment/addcomment', commentsService.addComment)
router.post('/comment/updatecomment', commentsService.updateComment)
router.post('/comment/deletecomment', commentsService.deleteComment)


module.exports = router