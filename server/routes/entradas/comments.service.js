const { Comment, User } = require('../../db')

//getPostComments(post_id) GET
getPostComments = (req, res, next) => {
    const { resp_id } = req.query
    Comment.findAll({
        include: [User],
        where: {
            postId: resp_id,
            deleted: false
        }
    }).then(respuestas => res.json(respuestas))
}


//addComment(post_id, user_id, body) POST
addComment = (req, res, next) => {
    const { resp_id, body, userId } = req.body
    Comment.create({
        userId,
        postId: resp_id,
        body,
        deleted: false
    }).then(() => res.send("1")).catch((err) => res.send("0"))
}

//updateComment (id, body)  POST
updateComment = (req, res, next) => {
    const { id, body } = req.body
    console.log("in update")
    Comment.update({ body }, { where: { id: id } }).then(() => res.send("1")).catch((err) => res.send("0"))
}

//deleteComment ( id ) POST
deleteComment = (req, res, next) => {
    const { id } = req.body
    console.log('on delete')
    Comment.update({ deleted: true }, { where: { id } }).then(() => res.send("1")).catch(err => res.send("0"))
}

module.exports = {
    getPostComments,
    addComment,
    updateComment,
    deleteComment,
}