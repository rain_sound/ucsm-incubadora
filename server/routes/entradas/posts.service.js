const { Post, User } = require('../../db')
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const multer = require("multer");

const nodemailer = require("nodemailer");

const { upload } = require("../../utilities");
const config = require("../../config");

//getAllPosts() GET
getAllPosts = (req, res, next) => {
        const { offset, limit } = req.query
        console.log(offset);
        console.log(limit);
        Post.findAll({
            order: [
                ['updatedAt', 'DESC']
            ],
            offset,
            limit,
            include: [User],
            where: { 
                deleted: false,
                mark: false,
            },

        }).then(posts => res.json(posts))
    }
    //getAllPostsSize() GET
getAllPostsSize = (req, res, next) => {
    Post.count({ where: { deleted: false } }).then(c => {
        let data = {
            size: c
        }
        res.send(data)
    })
}

//getUserPosts(user_id) GET
getUserPosts = (req, res, next) => {
        const { userId, offset, limit } = req.query
        Post.findAll({
            order: [
                ['updatedAt', 'DESC']
            ],
            offset,
            limit,
            include: [User],
            where: {
                userId,
                deleted: false
            }
        }).then(posts => res.json(posts))
    }
    //getPostsSize() GET
getUserPostsSize = (req, res, next) => {
    const { userId } = req.query
    Post.count({
        where: {
            userId,
            deleted: false
        }
    }).then(c => {
        let data = {
            size: c
        }
        res.send(data)
    })
}

//regPost(user_id, head, body) POST
regPost = (req, res, next) => {
    const { userId, head, body } = req.body
    Post.create({
        userId,
        head,
        body,
        deleted: false,
        mark: false,
        postimg: 'default.png'
    }).then(() => res.send("1")).catch((err) => res.send("0"))
}

//getPostById(id) GET
getPostById = (req, res, next) => {
    const { id } = req.query
    Post.findOne({
        include: [User],
        where: {
            deleted: false,
            id
        }
    }).then(posts => res.json(posts))
}

//updatePost (id, head, body)  POST
updatePost = (req, res, next) => {
    const { id, head, body } = req.body
    console.log("in update")
    Post.update({ head, body }, { where: { id: id } }).then(() => res.send("1")).catch((err) => res.send("0"))
}

//deletePost ( id ) POST
deletePost = (req, res, next) => {
    const { id } = req.body
    console.log('on delete')
    Post.update({ deleted: true }, { where: { id } }).then(() => res.send("1")).catch(err => res.send("0"))
}


//subir imagen al post
function uploadImage(req, res, next) {

    upload(req, res, err => {
        let { id } = req.body
        let postimg = req.file.filename
        if (err) {
            if (err instanceof multer.MulterError || err.code === 'LIMIT_FILE_SIZE') {
                return sendJson(413, "Archivo muy grande")
            } else {
                return sendJson(413, "Formato no permitido")
            }
        } else {
            Post.findOne({
                where: {
                    id
                }
            }).then(post => {
                if (post) {
                    post.update({
                        postimg
                    })
                } else sendJson(404, "El post no existe")
            }).catch(err => sendJson(500, err))
        }
        return res
            .status(200)
            .json({
                message: "Se actualizo la imagen con exito",
                filename: req.file.filename
            });
    });
    const sendJson = (code, reason) => {
        return res.status(code).json({ error: true, reason: reason });
    };
}


module.exports = {
    getAllPosts,
    getAllPostsSize,
    getUserPosts,
    getUserPostsSize,
    regPost,
    getPostById,
    updatePost,
    deletePost,
    uploadImage
}