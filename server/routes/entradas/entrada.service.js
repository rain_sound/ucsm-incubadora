// const { Entrada } = require('../../db')

// //  metodos CRUD
// //  inserciones
// //      insertar una entrada nueva POST
// crearEntrada = (req, res, next) => {
//     const { head, body, user_id } = req.body
//     Entrada.create({
//         user_id,
//         head,
//         body,
//         tipo: true
//     }).then(() => res.send("1")).catch((err) => res.send("0"))
// }

// //      insertar respuesta nueva POST
// ingresarRespuesta = (req, res, next) => {
//     const { resp_id, body, user_id } = req.body
//     Entrada.create({
//         user_id,
//         resp_id,
//         body,
//         tipo: false
//     }).then(() => res.send("1")).catch((err) => res.send("0"))
// }

// //  obtener las listas de elementos
// //      Listar todas entradas GET
// listarEntradas = (req, res, next) => {
//     Entrada.findAll({
//         where: { tipo: true }
//     }).then(entradas => res.json(entradas))
// }

// //      Listar entradas de un solo usuario GET
// listarEntradasUsuario = (req, res, next) => {
//     const { user_id } = req.query
//     Entrada.findAll({
//         where: {
//             tipo: true,
//             user_id
//         }
//     }).then(entradas => res.json(entradas))
// }

// //      Listar respuestas segun el cogigo de la entrada GET
// listarRespuestas = (req, res, next) => {
//     const { resp_id } = req.query
//     Entrada.findAll({
//         where: {
//             tipo: false,
//             resp_id
//         }
//     }).then(respuestas => res.json(respuestas))
// }

// //Update Entry POST
// actualizarEntrada = (req, res, next) => {
//     const { id, head, body } = req.body
//     console.log("in update")
//     Entrada.update({ head, body }, { where: { id: id } }).then(() => res.send("1")).catch((err) => res.send("0"))
// }

// //Delete Entry POST
// borrarEntrada = (req, res, next) => {
//     const { id } = req.body
//     console.log('on delete')
//     Entrada.update({ deleted: true }, { where: { id } }).then(() => res.send("1")).catch(err => res.send("0"))
// }


// module.exports = {
//     crearEntrada,
//     ingresarRespuesta,
//     listarEntradas,
//     listarEntradasUsuario,
//     listarRespuestas,
//     actualizarEntrada,
//     borrarEntrada
// }