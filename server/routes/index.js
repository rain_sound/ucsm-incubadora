var router = require('express').Router()
    //Definir rutas
const { verifyToken } = require("./middleware")
const users = require('./users')
const courses = require('./courses')
const entradas = require('./entradas')
const admin = require('./admin')
const role = require('./roles')


//Controladores
router.use('/users', users)
/* router.use('/courses', verifyToken, courses) */
router.use('/courses', courses)
router.use('/entradas', entradas)
router.use('/admin', admin)
router.use('/role', role)

module.exports = router