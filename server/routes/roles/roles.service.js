const { Role } = require('../../db')

getAllRoles = (req, res, next) => {
    Role.findAll({ attributes: ['id', 'description'] }).then((roles) => res.json(roles))
}

module.exports = {
    getAllRoles
}