const multer = require("multer");
const router = require("express").Router();

const userService = require("./user.service");
const { verifyToken } = require("../middleware");
const { upload } = require("../../utilities");

router.get("/resetpassword", userService.resetPassword);
router.get("/getcountusers", userService.countUserToApprov);
router.post("/getuserspermissions", userService.getUsersPermissions);

router.post("/forgotpassword", userService.forgotPassword);
router.put("/updatepassword", userService.updatePassword);

router.get("/", userService.getAll);
router.get("/:id", verifyToken, userService.getById);
router.post("/signin", userService.signin);
router.post("/signup", userService.signup);
router.post("/requestpermissions", userService.requestPermissions);
router.post("/getusersfilter", userService.getFilterPermissions);

router.post("/uploadImage", userService.uploadImage);
// router.post("/uploadImage", function (req, res, next) {
//     upload(req, res, err => {
//         console.log("ERORRRRRRRRRRRR TIENE QUE ENTRAR AQUI PARA CONTROLARLO");
//         if (err instanceof multer.MulterError || !req.file) {
//           return sendJson(413, "Error al intentar subir el archivo");
//         }
//         return res.status(200).json({ filename: req.file.filename });
//       });
//       const sendJson = (code, reason) => {
//         return res.status(code).json({ error: true, reason: reason });
//       };
//   })

module.exports = router;
